require import AllCore Int IntDiv List FSet FMap Distr DBool. 
require import PROM.
require (*--*) Group ZModP Signatures2RO.
require (*--*) FelTactic.
(*---*) import StdOrder.RealOrder StdBigop.Bigreal.BRA.

pragma +implicits.
pragma -oldip.
 
(** Setting:
    - a prime order cyclic group,
    - exponents interpreted in Z/pZ.
**)
clone import Group.CyclicGroup
rename [op] "order" as "p"
       [op] "g"     as "gg".

axiom prime_p: prime p.

clone import PowZMod as GP with
  lemma prime_order <- prime_p
rename
         "dunifin" as "dZp"
  [type] "exp" as "zmod".
import ZModE ZModE.DZmodP.

module type CDH_Adv = {
  proc solve(ga : group, gb : group) : group
}.

module CDH (A : CDH_Adv) = {
  proc run() = {
    var a, b, r;

    a <$ dZp;
    b <$ dZp;
    r <@ A.solve(gg ^ a, gg ^ b);
    return r = gg ^ (a * b);
  }
}.

(** This allows us to sample uniformly in our group **)
op dgp = dmap dZp (fun (x : zmod)=> gg ^ x).

(* The distribution defined above is the uniform distribution over the
   group.
*)
lemma dgp_ll: is_lossless dgp.
proof. by rewrite dmap_ll dZp_ll. qed.

lemma dgp_uni: is_uniform dgp.
proof.
rewrite dmap_uni 2:dZp_uni.
by move=> x y /= /(congr1 logge); rewrite !loggK.
qed.

lemma dgp_fu: is_full dgp.
proof.
rewrite dmap_fu 2:dZp_fu.
by move=> y; exists (logge y); rewrite expgK.
qed.

lemma dgp1E g: mu1 dgp g = inv p%r.
proof.
rewrite (@dmap1E_can _ _ logge g).
+ by move=> x; rewrite expgK.
+ by move=> x _ /=; rewrite loggK.
by rewrite dZp1E cardE.
qed.

(* Signatures *)
type msg.

clone include Signatures2RO with
  type      skey <- zmod,
  type      pkey <- group,
  type       msg <- msg,
  type signature <- group * (zmod * zmod) * bool,
  type    ro_in1 <- msg * bool,
  type   ro_out1 <- group,
    op      d1 _ <- dgp,
  type    ro_in2 <- group * group * group * group * msg,
  type   ro_out2 <- zmod,
    op      d2 _ <- dZp.

(* The Scheme (as a stateful probabilistic scheme; insert a PRF that
   generates b and r for a stateless deterministic scheme)
*) 
module (GJKW : Sig_Scheme) (H : Ht.RO) (G : Gt.RO) = {
  var map : (msg, group * (zmod * zmod) * bool) fmap

  proc keygen() = {
    var sk;

    map <- empty;
     sk <$ dZp;
    return (gg ^ sk, sk);
  }

  proc sign(sk : zmod, m) = {
    var b, h, r, c;

    if (m \notin map) {
      b <$ {0,1};
      h <@ H.get(m, b);

      r <$ dZp;
      c <@ G.get(h, h ^ sk, gg ^ r, h ^ r, m);
      map.[m] <- (h ^ sk, (c, c * sk + r), b);
    }
    return oget map.[m];
  }

  proc verify(pk : group, m, sig) = {
    var y, pi, b, h, c';
    var c, s : zmod;

    (y, pi, b) <- sig;
        (c, s) <- pi;
             h <@ H.get(m, b);
            c' <@ G.get(h, y, gg ^ s * pk ^ -c, h ^ s * y ^ -c, m);
    return c = c';
  }
}.

(* The simulator and solver *)
type h_resp =
[ AHQuery of zmod
| SHQuery of zmod ].

op get_h (gb : group) d =
with d = AHQuery d => gb * gg ^ d
with d = SHQuery d => gg ^ d.

op get_d x =
with x = AHQuery d => d
with x = SHQuery d => d. 

module Simulator (A : CMA_Adv): CDH_Adv  = {
  var pk : group
  var gb : group

  var ms : (msg, group * (zmod * zmod) * bool) fmap
  var hm : (msg * bool, h_resp) fmap
  var gm : (group * group * group * group * msg, zmod) fmap

  
  proc compute_related(m) = {
    var b, c, d, h, s, y, u, v;
  
    if (m \notin ms) {
                         b <$ dbool;
                         s <$ dZp;
                         d <$ dZp;
               hm.[(m, b)] <- SHQuery d;
                         h <- gg ^ d;
  
                         c <$ dZp;
                         y <- pk ^ d;
                         u <- gg ^ s * pk ^ -c;
                         v <- h ^ s * y ^ -c;
      gm.[(h, y, u, v, m)] <- c;
       
                    ms.[m] <- (y, (c, s), b);
    }
  }

  proc h(x) = {
    var y;

    y <$ dZp;
    if (x \notin hm) {
      hm.[x] <- AHQuery y;
    }
    return get_h gb (oget hm.[x]);
  }

  proc g(x) = {
    var y;

    y <$ dZp;
    if (x \notin gm) {
      gm.[x] <- y;
    }
    return oget gm.[x];
  }

  module O = {
    proc h(m, b) = {
      var r;
  
           compute_related(m);
      r <@ h(m, b);
      return r;
    }
  
    proc g(h, y, a, b, m) = {
      var r;
 
           compute_related(m); 
      r <@ g(h, y, a, b, m);
      return r;
    }
  
    proc sign(m) = {
      compute_related(m);
      return oget ms.[m];
    }
  }

  proc solve(ga', gb') = {
    var m, sig, y, pi, b, c, s, d;

    pk <- ga';
    gb <- gb';

    ms <- empty;
    hm <- empty;
    gm <- empty;
    
    (m, sig) <@ A(O).forge(pk);
    (y, pi, b) <- sig;
    (c, s) <- pi;

    d <$ dZp;
    d <- oapp get_d d hm.[m, b];
    return y * pk ^ -d;
  }
}.

module (CountingA (A : CMA_Adv) : CMA_Adv) (O : CMA_Oracles) = {
  var cG : int

  module Go = {

    proc sign = O.sign
    proc h = O.h
    proc g(x) = {
      var r <- witness;

      r   <@ O.g(x);
      cG  <- cG + 1;
      return r;
    }
  }

  proc forge(pk) = {
    var r;

    cG  <- 0;
    r   <@ A(Go).forge(pk);
    return r; 
  } 
}.

section.
(* FIXME: Locally clone Gt and Ht to hide restriction on the FROs *)
declare module A <: CMA_Adv { -Ht.RO, -Gt.RO, -Ht.FRO, -Gt.FRO, -CountingA, -GJKW, -EUF_CMA, -Simulator }.

declare axiom A_forge_ll (O <: CMA_Oracles { -A }):
     islossless O.h
  => islossless O.g
  => islossless O.sign
  => islossless A(O).forge.

local clone PROM.FullRO as Bt with
  type    in_t <- msg,
  type   out_t <- bool,
  type  d_in_t <- unit,
  type d_out_t <- bool,
    op  dout _ <- dbool
proof *. 

local clone PROM.FullRO as Rt with
  type    in_t <- msg,
  type   out_t <- zmod,
  type  d_in_t <- unit,
  type d_out_t <- bool,
    op  dout _ <- dZp
proof *.

local module Game0 (B : Bt.RO) (H : Ht.RO) (G : Gt.RO) = {
  var sk : zmod
  var qs : msg fset

  var  m : msg
  var  y : group
  var  c : zmod
  var  s : zmod
  var  b : bool
  var  h : group

  module O = {
    proc h = H.get
    proc g = G.get

    proc sign(m) = {
      var b, h, r, c;

          qs <- qs `|` fset1 m;
           b <@ B.get(m);
           r <@ Rt.RO.get(m);
           h <@ H.get(m, b);
           c <@ G.get(h, h ^ sk, gg ^ r, h ^ r, m);
      return (h ^ sk, (c, c * sk + r), b);
    }
  }

  proc run() = {
    var pk, sig, pi, c';

                  H.init();
                  G.init();
                  B.init();
                  Rt.RO.init();
            qs <- fset0;

            sk <$ dZp;
            pk <- gg ^ sk;
 CountingA.cG  <- 0;
      (m, sig) <@ A(O).forge(pk);

    (y, pi, b) <- sig;
        (c, s) <- pi;
             h <@ H.get(m, b);
            c' <@ G.get(h, y, gg ^ s * pk ^ -c, h ^ s * y ^ -c, m);
                  B.sample(m);

    return c = c' /\ !m \in qs;
  }

  proc distinguish = run (* Fit the RO distinguisher interface *)
}.

local equiv Step0_h x0: Ht.RO.get ~ Ht.RO.get:
     ={glob Ht.RO, x}
  /\ x{1} = x0
  /\ (forall m y c s b,
           GJKW.map{1}.[m] = Some (y, (c, s), b)
        =>    Bt.RO.m{2}.[m] = Some b
           /\ Rt.RO.m{2}.[m] = Some (s - c * Game0.sk{2})
           /\ exists h,
                   y = h ^ Game0.sk{2}
                /\ Ht.RO.m{2}.[m, b] = Some h
                /\ Gt.RO.m{2}.[h, y, gg ^ (s - c * Game0.sk{2}), h ^ (s - c * Game0.sk{2}), m] = Some c)
  ==>    ={glob Ht.RO, res}
      /\ Ht.RO.m{1}.[x0] = Some res{1}
      /\ (forall m y c s b,
               GJKW.map{1}.[m] = Some (y, (c, s), b)
            =>    Bt.RO.m{2}.[m] = Some b
               /\ Rt.RO.m{2}.[m] = Some (s - c * Game0.sk{2})
               /\ exists h,
                       y = h ^ Game0.sk{2}
                    /\ Ht.RO.m{2}.[m, b] = Some h
                    /\ Gt.RO.m{2}.[h, y, gg ^ (s - c * Game0.sk{2}), h ^ (s - c * Game0.sk{2}), m] = Some c).
proof.
proc; auto=> /> &1 &2 ih h _; rewrite !domE=> /=.
case _: (Ht.RO.m.[x0]{2})=> /> h_x.
rewrite get_set_sameE /=.
move=> m y c s b /ih /> {y} b_m r_m h0 h_mb g_yhabm.
by exists h0=> //=; rewrite g_yhabm /= get_set_neqE /#.
qed.

local equiv Step0_g x0: Gt.RO.get ~ Gt.RO.get:
     ={glob Gt.RO, x}
  /\ x{1} = x0
  /\ (forall m y c s b,
           GJKW.map{1}.[m] = Some (y, (c, s), b)
        =>    Bt.RO.m{2}.[m] = Some b
           /\ Rt.RO.m{2}.[m] = Some (s - c * Game0.sk{2})
           /\ exists h,
                   y = h ^ Game0.sk{2}
                /\ Ht.RO.m{2}.[m, b] = Some h
                /\ Gt.RO.m{2}.[h, y, gg ^ (s - c * Game0.sk{2}), h ^ (s - c * Game0.sk{2}), m] = Some c)
  ==>    ={glob Gt.RO, res}
      /\ Gt.RO.m{1}.[x0] = Some res{1}
      /\ (forall m y c s b,
               GJKW.map{1}.[m] = Some (y, (c, s), b)
            =>    Bt.RO.m{2}.[m] = Some b
               /\ Rt.RO.m{2}.[m] = Some (s - c * Game0.sk{2})
               /\ exists h,
                       y = h ^ Game0.sk{2}
                    /\ Ht.RO.m{2}.[m, b] = Some h
                    /\ Gt.RO.m{2}.[h, y, gg ^ (s - c * Game0.sk{2}), h ^ (s - c * Game0.sk{2}), m] = Some c).
proof.
proc; auto=> /> &1 &2 ih c _; rewrite !domE=> /=.
case _: (Gt.RO.m{2}.[x0])=> /> h_x.
rewrite get_set_sameE /=.
move=> m y c0 s b /ih /> {y} b_m r_m h h_mb g_yhabm.
by exists h=> //=; rewrite h_mb /= get_set_neqE /#.
qed.

local equiv Step0:
  EUF_CMA(Ht.RO, Gt.RO, GJKW, A).run ~ Game0(Bt.LRO, Ht.RO, Gt.RO).run:
    ={glob A} ==> ={res}.
proof.
proc.
inline {1} 6.
seq  5   9: (   ={glob Ht.RO, glob Gt.RO, pk, sig}
            /\ ={qs}(EUF_CMA, Game0)
            /\ m{1} = Game0.m{2}); last first. 
+ by wp; inline {2} 5; sim.
call (:    ={glob Ht.RO, glob Gt.RO}
        /\ ={qs, sk}(EUF_CMA, Game0)
        /\ (forall m, GJKW.map{1}.[m] = None <=> Bt.RO.m{2}.[m] = None)
        /\ (forall m, GJKW.map{1}.[m] = None <=> Rt.RO.m{2}.[m] = None)
        /\ (forall m y c s b,
                 GJKW.map{1}.[m] = Some (y, (c, s), b)
              =>    Bt.RO.m{2}.[m] = Some b
                 /\ Rt.RO.m{2}.[m] = Some (s - c * Game0.sk{2})
                 /\ exists h,
                         y = h ^ Game0.sk{2}
                      /\ Ht.RO.m{2}.[m, b] = Some h
                      /\ Gt.RO.m{2}.[h, y, gg ^ (s - c * Game0.sk{2}), h ^ (s - c * Game0.sk{2}), m] = Some c)).
+ by exists* arg{1}; elim * => x; conseq (Step0_h x).
+ by exists* arg{1}; elim * => x; conseq (Step0_g x).
+ proc; inline {1} 2; inline {2} 3; inline {2} 2.
  case: ((m \in GJKW.map){1}).
  + rcondf {1}  4; first by auto.
    rcondf {2}  4; first by auto=> /#.
    rcondf {2}  7; first by auto=> /#.
    inline {2}  9; inline {2} 8.
    rcondf {2} 10.
    + auto=> /> &0 eq_domb eq_domr fun_rel + _ _ _ _; rewrite !domE.
      case _: (GJKW.map.[m]{m})=> /> [] y [] c s b /fun_rel [] /> {y}.
      by move=> -> + h ->.
    rcondf {2} 13.
    + auto=> /> &0 eq_domb eq_domr fun_rel + _ _ _ _ _ _; rewrite !domE.
      case _: (GJKW.map.[m]{m})=> /> [] y [] c s b /fun_rel [] /> {y}.
      by move=> -> -> h -> /= ->.
    auto=> /> &1 &2 eq_domb eq_domr fun_rel; rewrite !domE dgp_ll=> /= + _ _ _ _ _ _ _ _.
    case _: (GJKW.map{1}.[m{2}])=> /> [] y [] c s b /fun_rel /> {y}.
    move=> -> -> h -> /= -> /=.
    by field.
  rcondt {1}  4; first by auto.
  rcondt {2}  4; first by auto=> /#.
  rcondt {2}  8; first by auto=> /#.
  swap {1} 6 -1.
  wp.
  wp; ecall (Step0_g (h, h ^ sk, gg ^ r0, h ^ r0, m0){1}).
  wp; ecall (Step0_h (m0, b){1}).
  auto=> /> &1 &2 eq_domb eq_domr fun_rel; rewrite !domE /=.
  move=> m_notin_map b _ r _; rewrite !get_set_sameE /=.
  split.
  + move=> m0 y0 c0 s0 b0.
    by case: (m0 = m{2})=> [/#|neq_m]; rewrite !get_set_neqE=> // /fun_rel.
  move=> Hb h Hm h_mb Hh c Gm g_yhabm Hg.
  rewrite !get_set_sameE //=.
  split.
  + by move=> m0; rewrite !get_setE; case: (m0 = m{2})=> />; rewrite eq_domb.
  split.
  + by move=> m0; rewrite !get_setE; case: (m0 = m{2})=> />; rewrite eq_domr.
  move=> m0 y0 c0 s0 b0; rewrite get_setE; case: (m0 = m{2})=> />; last first.
  + by move=> _; exact: Hg.
  rewrite !get_set_sameE=> />.
  split; 1:by field.
  rewrite -ZModpField.addrA ZModpField.addrC ZModpField.subrK //=.
  by exists h.
by inline *; auto=> /> sk _; smt(emptyE).
qed.

(** Simplification 1: ignore forgeries due to NIZK unsoundness *)
(* This one requires a bit of work. We need to:
   - reduce the final event to an in-oracle bad event;
   - set up query counters;
   - bound the probability using the FEL. *)
declare op qG : { int | 0 <= qG } as ge0_qG.
declare axiom A_bounded c (O <: CMA_Oracles { -A }): hoare [A(CountingA(A, O).Go).forge: CountingA.cG = c ==> CountingA.cG <= c + qG].

local module Game0_bad (B : Bt.RO) (H : Ht.RO) (G : Gt.RO) = {
  var sk : zmod
  var qs : msg fset
  var bad : bool

  var  m : msg
  var  y : group
  var  c : zmod
  var  s : zmod
  var  b : bool
  var  h : group

  module O = {
    proc h = H.get
    proc g(h, y, a, b, m) = {
      var c;

       c <$ dZp;
       if ((h, y, a, b, m) \notin Gt.RO.m) {
             bad <- bad \/ (   y <> h ^ sk
                            /\ (a * gg ^ sk ^ c) ^ logge h = b * y ^ c);
         Gt.RO.m.[(h, y, a, b, m)] <- c;
       }
       return oget Gt.RO.m.[h, y, a, b, m];
    } 

    proc sign(m) = {
      var b, h, r, c;

          qs <- qs `|` fset1 m;
           b <@ B.get(m);
           r <@ Rt.RO.get(m);
           h <@ H.get(m, b);
           c <@ G.get(h, h ^ sk, gg ^ r, h ^ r, m);
      return (h ^ sk, (c, c * sk + r), b);
    }
  }

  proc run() = {
    var pk, sig, pi, c';

                  H.init();
                  G.init();
                  B.init();
                  Rt.RO.init();
            qs <- fset0;
           bad <- false;

            sk <$ dZp;
            pk <- gg ^ sk;
 CountingA.cG  <- 0;
      (m, sig) <@ A(CountingA(A,O).Go).forge(pk);

    (y, pi, b) <- sig;
        (c, s) <- pi;
             h <@ H.get(m, b);
            c' <@ CountingA(A,O).Go.g(h, y, gg ^ s * pk ^ -c, h ^ s * y ^ -c, m);
                  B.sample(m);

    return c = c' /\ !m \in qs;
  }

  proc distinguish = run (* Fit the RO distinguisher interface *)
}.

local lemma pr_Game0_bad &m:
    Pr[Game0(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0.y <> Game0.h ^ Game0.sk]
  = Pr[Game0_bad(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0_bad.y <> Game0_bad.h ^ Game0_bad.sk /\ CountingA.cG <= qG + 1]. 
proof.
have ->: Pr[Game0(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0.y <> Game0.h ^ Game0.sk]
       = Pr[Game0_bad(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0_bad.y <> Game0_bad.h ^ Game0_bad.sk].
+ byequiv (: ={glob A} ==> _)=> //; proc.
  call (: ={glob Bt.RO}); 1:by sim.
  call (: ={glob Gt.RO}); 1:by inline *; auto=> /#.
  call (: ={glob Ht.RO}); 1:by sim.
  wp; call (:    ={glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO}
              /\ ={sk, qs}(Game0, Game0_bad)).
  + by sim.
  + by proc; inline *; auto=> /> &2; case: (x{2}).
  + by sim.
  by sim: (   ={glob A, glob Ht.RO, glob Gt.RO, glob Bt.RO, glob Rt.RO, pk}
           /\ ={qs, sk}(Game0, Game0_bad)).
byequiv=> //.
conseq (: ={res, Game0_bad.y, Game0_bad.h, Game0_bad.sk, CountingA.cG}) (: _ ==> CountingA.cG <= qG + 1)=> //=.
+ proc; inline *; auto.
  call (A_bounded 0 (<: Game0_bad(Bt.LRO, Ht.RO, Gt.RO).O)).
  by inline *; auto=> /#.
by sim.
qed.

local lemma pr_bady_bad &m:
     Pr[Game0_bad(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0_bad.y <> Game0_bad.h ^ Game0_bad.sk /\ CountingA.cG <= qG + 1]
  <= Pr[Game0_bad(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: Game0_bad.bad /\ CountingA.cG <= qG + 1].
proof.
byequiv (: ={glob A} ==> _)=> //=. 
conseq (: ={glob A} ==> ={glob Ht.RO, glob Gt.RO, CountingA.cG, Game0_bad.bad, res})
       (: true ==> res => Game0_bad.y <> Game0_bad.h ^ Game0_bad.sk => Game0_bad.bad);
  [1:smt()|3:by sim].
proc; inline *; auto=> //=.
call (:    (exists h y a b m (c : zmod),
                 Gt.RO.m.[h, y, a, b, m] = Some c
              /\ y <> h ^ Game0_bad.sk
              /\ (a * gg ^ Game0_bad.sk ^ c) ^ logge h = b * y ^ c)
        => Game0_bad.bad).
+ by conseq (: true).
+ proc; inline *; auto=> /> &0 ih c _; case: (x{0})=> /= h y a b m.
  rewrite domE=> /= g_hyabm h' y' a' b' m' c'; rewrite get_setE.
  case: ((h', y', a', b', m') = (h, y, a, b, m))=> /> _ h1 h2 h3.
  move: (ih _); 1:by exists h' y' a' b' m' c'.
  by move=> ->.
+ proc; seq  4: #pre.
  + by conseq (: true).
  inline *; auto=> /> &0 ih c _; rewrite domE=> /= g_hyabm h' y' a' b' m' c'.
  rewrite get_setE; case: ((h', y', a', b', m') = (h, h ^ Game0_bad.sk, gg ^ r, h ^ r, m){0})=> />.
  by move=> _ h1 h2 h3; move: (ih _); 1:by exists h' y' a' b' m' c'.
auto=> /> sk _.
split=> [|_]; 1:smt(emptyE).
move=> [] m [] y [] c s b bad qs gM hM ih h _ /=; rewrite !domE !get_set_sameE /=.
case _: (hM.[m, b])=> />.
+ move=> hM_mb c' _; rewrite !get_set_sameE /=.
  case _: (gM.[h, y, gg ^ s * gg ^ sk ^ -c, h ^ s * y ^ -c, m])=> />.
  + move=> _ _ _; right.
    rewrite -!expM -!expD -mulcA -expD.
    have ->: s + sk * -c' + sk * c' = s by field.
    have ->: -c' + c' = zero by field.
    by rewrite exp0 mulc1 -expM ZModpField.mulrC expM expgK.
  move=> gM_hyabm _ bady; move: (ih _)=> //.
  exists h y (gg ^ s * gg ^ sk ^ -c) (h ^ s * y ^ -c) m c.
  rewrite gM_hyabm bady //=.
  rewrite -!expM -!expD -mulcA -expD.
  have ->: s + sk * -c + sk * c = s by field.
  have ->: -c + c = zero by field.
  by rewrite exp0 mulc1 -expM ZModpField.mulrC expM expgK.  
move=> {h} h hM_mb c' _; rewrite !get_set_sameE /=.
case _: (gM.[h, y, gg ^ s * gg ^ sk ^ -c, h ^ s * y ^ -c, m])=> />.
+ move=> _ _ _; right.
  rewrite -!expM -!expD -mulcA -expD.
  have ->: s + sk * -c' + sk * c' = s by field.
  have ->: -c' + c' = zero by field.
  by rewrite exp0 mulc1 -expM ZModpField.mulrC expM expgK.
move=> gM_hyabm _ bady; move: (ih _)=> //.
exists h y (gg ^ s * gg ^ sk ^ -c) (h ^ s * y ^ -c) m c.
rewrite gM_hyabm bady //=.
rewrite -!expM -!expD -mulcA -expD.
have ->: s + sk * -c + sk * c = s by field.
have ->: -c + c = zero by field.
by rewrite exp0 mulc1 -expM ZModpField.mulrC expM expgK.  
qed.

local phoare pr_Gbad: [Game0_bad(Bt.LRO, Ht.RO, Gt.RO).O.g: !Game0_bad.bad ==> Game0_bad.bad] <= (1%r / p%r).
proof.
proc=> />.
case: (((h, y, a, b, m) \in Gt.RO.m){hr}).
+ rcondf 2=> />; first by auto.
  by hoare=> //= _ _; rewrite invr_ge0 le_fromint; smt(ge2_p).
  rcondt 2=> />; first by auto.
  wp; rnd (fun (c : zmod)=>
              y <> h ^ Game0_bad.sk
           /\ (a * gg ^ Game0_bad.sk ^ c) ^ logge h = b * y ^ c)=> />. 
  skip=> //= . move=> /> &hr. 
  + case: (h{hr}, y{hr}, a{hr}, b{hr}, m{hr})=>  h' y' a' b' m' /= notBad _ .  
    apply/(ler_trans (mu1 dZp (((logge b) + (-(logge h) * (logge a))) * inv ((logge h) * (logge (gg^ Game0_bad.sk{hr})) + (- (logge y)))){hr})). 
  + rewrite mu_sub => @/pred1 c [#] /= y_neq_hsk => />.
    rewrite !loggK. 
    pose lh := logge h{hr}. pose ly := logge y{hr}.
    pose la := logge a{hr}. pose lb := logge b{hr}.
    move=> /(congr1 (logge)). 
    rewrite !(loggK, logrzM, logDr) ?g_n  // -/ly -/la -/lb.
    rewrite ZModpRing.mulrDr ZModpRing.addrC -ZModpRing.eqr_sub.
    rewrite -ZModpRing.mulrN ZModpRing.mulrCA -ZModpRing.mulrDr.
    move=> <-. rewrite -ZModpRing.mulrA ZModpField.mulrV.
    + move: y_neq_hsk; apply/contra.
      rewrite ZModpRing.addr_eq0 ZModpRing.opprK=> /(congr1 (fun (x : zmod)=> gg ^ x)) /=.
      move: expgK =>  expgK=> //=.  
      by rewrite  expM  /lh  /ly /la /lb expgK; smt().
  by rewrite ZModpRing.mulr1.
by rewrite dZp1E cardE=> />.
qed.

(*** Bounding the bad ***)
local lemma pr_Game0 &m:
   Pr[Game0_bad(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: Game0_bad.bad /\ CountingA.cG <= qG + 1] <= (qG + 1)%r / p%r.
proof.
fel 9 CountingA.cG 
      (fun _=> inv p%r)
      (qG + 1)
      Game0_bad.bad 
      [Game0_bad(Bt.LRO, Ht.RO, Gt.RO).O.sign: false ]=> //.
+ by rewrite StdBigop.Bigreal.sumr_const count_predT  size_range /=; smt(ge0_qG ge2_p)=> //=.
+ by inline*; auto=> />; smt(ge0_qG).
+ by proc; wp; call pr_Gbad; auto.
by move=> c0; proc; inline *; auto=> /#.
qed.

local lemma Bad_Bound &m:
     Pr[Game0(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0.y <> Game0.h ^ Game0.sk]
  <=  (qG + 1)%r / p%r.
proof. 
have ->: Pr[Game0(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0.y <> Game0.h ^ Game0.sk]
    =  Pr[Game0_bad(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0_bad.y <> Game0_bad.h ^ Game0_bad.sk].
byequiv=> //=. proc. 
+ call (: true )=> //=.
+ call (: ={arg, Gt.RO.m} ==> ={res}).
  proc. inline*. auto=> /> &2. 
  case(x{2})=> />.
+ call (: ={arg, Ht.RO.m} ==> ={res}).
  by sim. 
+ wp. 
+ call (: ={glob Gt.RO, glob Ht.RO, glob Bt.RO, glob Rt.RO} /\ ={qs, sk}(Game0, Game0_bad))=> />.
  by proc; sim.
  proc. inline*. auto=> /> &2. 
  case(x{2})=> />.
  proc; auto; sim=> />. inline*; auto=> /#. 
have ->: Pr[Game0_bad(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0_bad.y <> Game0_bad.h ^ Game0_bad.sk ]
       = Pr[Game0_bad(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0_bad.y <> Game0_bad.h ^ Game0_bad.sk /\ CountingA.cG <= qG + 1].
byequiv=> //=.
conseq (: ={res, Game0_bad.y, Game0_bad.h, Game0_bad.sk, CountingA.cG} ) (: true==> CountingA.cG <= qG+1 )=>  />. 
+ proc. 
  inline*; auto=> //=. 
  + call (A_bounded 0 (<: Game0_bad(Bt.LRO, Ht.RO, Gt.RO).O))=> />.
    auto=> />; smt().
+ sim.
smt(pr_bady_bad  pr_Game0).
qed.

(** Simplification 2: ignore forgeries that use a b different from the
                      one that would be used by the signing oracle **)
(* This one feels like magic, but it's really not. The proof is kind
   of self-explanatory, and neat, except for the next thing... *)
local module Game0' = {
  module O = Game0(Bt.RO, Ht.RO, Gt.RO).O

  proc inner() = {
    var pk, sig, pi, c';

                            Ht.RO.init();
                            Gt.RO.init();
                            Bt.RO.init();
                            Rt.RO.init();
                Game0.qs <- fset0;

                  Game0.sk <$ dZp;
                        pk <- gg ^ Game0.sk;
            (Game0.m, sig) <@ A(O).forge(pk);

    (Game0.y, pi, Game0.b) <- sig;
        (Game0.c, Game0.s) <- pi;
                   Game0.h <@ Ht.RO.get(Game0.m, Game0.b);
                        c' <@ Gt.RO.get(Game0.h, Game0.y,
                                        gg ^ Game0.s * pk ^ -Game0.c, Game0.h ^ Game0.s * Game0.y ^ -Game0.c,
                                        Game0.m);

    return Game0.c = c' /\ !Game0.m \in Game0.qs;
  }

  proc run() = {
    var r;
    r <@ inner();
         Bt.RO.sample(Game0.m);
    return r;
  }
}.

local lemma unpredictable &m:
     Pr[Game0(Bt.RO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0.y = Game0.h ^ Game0.sk]
  <=   2%r
     * Pr[Game0(Bt.RO, Ht.RO, Gt.RO).run() @ &m:
               res
            /\ Game0.y = Game0.h ^ Game0.sk
            /\ Bt.RO.m.[Game0.m] <> Some Game0.b].
proof.
have /#: Pr[Game0(Bt.RO, Ht.RO, Gt.RO).run() @ &m:
                 res
              /\ Game0.y = Game0.h ^ Game0.sk
              /\ Bt.RO.m.[Game0.m] <> Some Game0.b]
      >=   inv 2%r
         * Pr[Game0(Bt.RO, Ht.RO, Gt.RO).run() @ &m:
                res /\ Game0.y = Game0.h ^ Game0.sk].
have ->: Pr[Game0(Bt.RO, Ht.RO, Gt.RO).run() @ &m :
                 res
              /\ Game0.y = Game0.h ^ Game0.sk
              /\ Bt.RO.m.[Game0.m] <> Some Game0.b]
       = Pr[Game0'.run() @ &m:
                 res
              /\ Game0.y = Game0.h ^ Game0.sk
              /\ Bt.RO.m.[Game0.m] <> Some Game0.b].
+ by byequiv=> //; proc; inline {2} 1; swap {2} 13 1; wp; sim.
byphoare (: glob A = (glob A){m} ==> _)=> //.
proc. 
seq  1: (r /\ Game0.y = Game0.h ^ Game0.sk)
        (Pr[Game0'.inner() @ &m: res /\ Game0.y = Game0.h ^ Game0.sk]) (inv 2%r)
        _ 0%r
        (r => Bt.RO.m.[Game0.m] = None).
+ inline *; wp 17.
  conseq (: !Game0.m \in Game0.qs => Bt.RO.m.[Game0.m] = None)=> //.
  auto=> />.
  call (: forall m, m \in Game0.qs <=> Bt.RO.m.[m] <> None).
  + by conseq (: true).
  + by conseq (: true).
  + proc; seq  2: #pre; last by conseq (: true).
    inline *; auto=> /> &0 ih b _; split.
    + by move=> _ m0; rewrite -domE mem_set !in_fsetU !in_fset1 ih !domE.
    move=> + m0; rewrite -domE !in_fsetU !in_fset1 ih !domE.
    by case: (m0 = m{0}).
  auto=> /> sk _; split=> [|_].
  + smt(emptyE in_fset0).
  by move=> [] m sig qs brM h _ _ _ _ /=; rewrite h. 
+ call (: (glob A){m} = (glob A) ==> res /\ Game0.y = Game0.h ^ Game0.sk)=> //.
  by bypr=> &0 eq_gA; byequiv (: ={glob A} ==> ={res, Game0.y, Game0.sk, Game0.h})=> //; sim.
+ inline *; rcondt  4; first by auto=> /#.
  wp; rnd (pred1 (!Game0.b)); auto=> /> &0 _ _; split.
  + by rewrite dbool1E.
  by move=> _ b _; rewrite get_set_sameE /#.
+ by hoare; conseq (: true)=> [/#|].
move=> &0 eq_gA; rewrite RField.mulrC StdOrder.RealOrder.lerr_eq.
congr; byequiv (: ={glob A} ==> ={res, Game0.sk, Game0.y, Game0.h})=> //.
proc. inline {1} 14; inline {1} 15.
wp 16 13; rnd {1}; wp 13 13.
conseq (: ={Game0.m, Game0.c, Game0.qs, Game0.sk, Game0.y, Game0.h, Bt.RO.m, Rt.RO.m, c'})=> />.
by sim.
qed.

local clone Bt.FullEager as BtE.
local module Game0F (B : Bt.RO) = Game0(B, Ht.RO, Gt.RO).

local lemma decompose &m:
     Pr[EUF_CMA(Ht.RO, Gt.RO, GJKW, A).run() @ &m: res]
  <=   (qG + 1)%r / p%r 
     + 2%r * Pr[Game0(Bt.RO, Ht.RO, Gt.RO).run() @ &m:
                     res
                  /\ Game0.y = Game0.h ^ Game0.sk
                  /\ Bt.RO.m.[Game0.m] <> Some Game0.b].
proof.
have ->: Pr[EUF_CMA(Ht.RO, Gt.RO, GJKW, A).run() @ &m: res]
       = Pr[Game0(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res].
+ by byequiv Step0.
rewrite Pr[mu_split (Game0.y <> Game0.h ^ Game0.sk)].
have ->: Pr[Game0(Bt.LRO, Ht.RO, Gt.RO).run() @ &m: res /\ !Game0.y <> Game0.h ^ Game0.sk]
       = Pr[Game0(Bt.RO, Ht.RO, Gt.RO).run() @ &m: res /\ Game0.y = Game0.h ^ Game0.sk].
+ rewrite eq_sym; byequiv (BtE.RO_LRO_D Game0F _)=> //.
  by move=> />; rewrite dbool_ll.
smt(Bad_Bound unpredictable).
qed.

(** Step 1: We anticipate signature computation on a message m
    everytime the adversary makes *any* query that contains m.

    We'll do this using the PROM theory—abstracting away details of
    eager sampling. We first eagerly sample b and r, then h, and
    finally c. We can't do this in one go because of dependencies in
    the computation.
**)
local module type CompRel (B : Bt.RO) (R : Rt.RO) (H : Ht.RO) (G : Gt.RO) = {
  proc compute_related(_ : msg): unit
}.

local module Game1 (C : CompRel) (B : Bt.RO) (R : Rt.RO) (H : Ht.RO) (G : Gt.RO) = {
  var sk : zmod
  var qs : msg fset

  var  m : msg
  var  y : group
  var  c : zmod
  var  s : zmod
  var  b : bool
  var  h : group

  var bm : bool

  module O = {
    proc h(m, b) = {
      var r;

           C(B, R, H, G).compute_related(m);
      r <@ H.get(m, b);
      return r;
    }

    proc g(h, y, a, b, m) = {
      var r;

           C(B, R, H, G).compute_related(m);
      r <@ G.get(h, y, a, b, m);
      return r;
    }

    proc sign(m) = {
      var b, h, r, c;

            C(B, R, H, G).compute_related(m);
      qs <- qs `|` fset1 m;
       b <@ B.get(m);
       r <@ R.get(m);
       h <@ H.get(m, b);
       c <@ G.get(h, h ^ sk, gg ^ r, h ^ r, m);
      return (h ^ sk, (c, c * sk + r), b);
    }
  }

  proc run() = {
    var pk, sig, pi, c';

                  H.init();
                  G.init();
                  B.init();
                  R.init();
            qs <- fset0;

            sk <$ dZp;
            pk <- gg ^ sk;
      (m, sig) <@ A(O).forge(pk);

    (y, pi, b) <- sig;
        (c, s) <- pi;
             h <@ H.get(m, b);
            c' <@ G.get(h, y, gg ^ s * pk ^ -c, h ^ s * y ^ -c, m);
            bm <@ B.get(m);

    return c = c' /\ !m \in qs;
  }

  proc distinguish = run (* Fit the RO distinguisher interface *)
}.

(** Everything before could be Game1(CR0, ...) with **)
local module CR0 (B : Bt.RO) (R : Rt.RO) (H : Ht.RO) (G : Gt.RO) = {
  proc compute_related(m : msg) = {  }
}.

(** Sub-step 1: Anticipate sampling of b and r **)
local module CR_b (B : Bt.RO) (R : Rt.RO) (H : Ht.RO) (G : Gt.RO) = {
  proc compute_related(m) = {
    B.sample(m);
    R.sample(m);
  }
}.

local module Game1_b (B : Bt.RO) = Game1(CR_b, B, Rt.LRO, Ht.LRO, Gt.LRO).
local module Game1_r (R : Rt.RO) = Game1(CR_b, Bt.RO, R, Ht.LRO, Gt.LRO).

local equiv Game0_Game1_b:
  Game0(Bt.RO, Ht.LRO, Gt.LRO).run ~ Game1(CR_b, Bt.RO, Rt.RO, Ht.LRO, Gt.LRO).run:
    ={glob A}
    ==>    ={glob A, res}
        /\ ={h, y, s, c, m, b, qs, sk}(Game0, Game1)
        /\ Bt.RO.m.[Game0.m]{1} = Some Game1.bm{2}.
proof.
transitivity Game1(CR_b, Bt.LRO, Rt.LRO, Ht.LRO, Gt.LRO).run
  (={glob A}
   ==>    ={glob A, res}
       /\ ={h, y, s, c, m, b, qs, sk}(Game0, Game1)
       /\ Bt.RO.m.[Game0.m]{1} = Some Game1.bm{2})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Gt.RO.m{2} Ht.RO.m{2} Rt.RO.m{2}.
+ proc.
  seq 13 12: (   ={glob A, glob Bt.RO, glob Rt.RO, c'}
              /\ ={c, m, qs, h, y, s, b, sk}(Game0, Game1)).
  + wp; call (: ={glob Gt.RO}); 1:by sim.
    call (: ={glob Ht.RO}); 1:by sim.
    wp; call (:    ={glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO}
                /\ ={sk, qs}(Game0, Game1)).
    + proc *; inline {2} 1; inline {2} 3; inline {2} 5; inline {2} 4.
      wp; call (: ={glob Ht.RO}); 1:by sim.
      by auto.
    + proc *; inline {2} 1; inline {2} 6; inline {2} 8; inline {2} 7.
      wp; call (: ={glob Gt.RO}); 1:by sim.
      by auto.
    + proc; inline {2} 1; inline {2} 3; inline {2} 2.
      by sim.
    by sim: (   ={glob A, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO, pk}
             /\ ={qs, sk}(Game0, Game1)).
  inline *; auto=> /> &2 b _; rewrite !domE /=.
  by case: (Bt.RO.m.[Game1.m]{2})=> />; rewrite get_set_sameE.
transitivity Bt.MainD(Game1_b, Bt.LRO).distinguish
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO} ==> ={res, glob Game1})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Gt.RO.m{2} Ht.RO.m{2} Rt.RO.m{2}.
+ proc *; inline {2} 1; wp.
  call (: ={glob A} ==> ={res, glob Game1, glob Bt.RO}); first by sim.
  inline *; auto=> />.
transitivity Bt.MainD(Game1_b, Bt.RO).distinguish
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO} ==> ={res, glob Game1})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Gt.RO.m{2} Ht.RO.m{2} Rt.RO.m{2}.
+ symmetry; conseq (Bt.FullEager.RO_LRO Game1_b _)=> />.
  by rewrite dbool_ll dZp_ll.
transitivity Rt.MainD(Game1_r, Rt.LRO).distinguish
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO} ==> ={res, glob Game1})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Gt.RO.m{2} Ht.RO.m{2} Rt.RO.m{2}.
+ by sim.
transitivity Rt.MainD(Game1_r, Rt.RO).distinguish
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO} ==> ={res, glob Game1})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Gt.RO.m{2} Ht.RO.m{2} Rt.RO.m{2}.
+ symmetry; conseq (Rt.FullEager.RO_LRO Game1_r _)=> />.
  exact: dZp_ll.
proc *; inline {1} 1; wp.
call (: ={glob A} ==> ={res, glob Game1}); first by sim.
by inline *; auto.
qed.

(** Sub-step 2: anticipate sampling of h **)
local module CR_h (B : Bt.RO) (R : Rt.RO) (H : Ht.RO) (G : Gt.RO) = {
  proc compute_related(m) = {
    var b, r;

    b <@ B.get(m);
    r <@ R.get(m);
         H.sample(m, b);
  }
}.

local module Game1_h (H : Ht.RO) = Game1(CR_h, Bt.RO, Rt.RO, H, Gt.LRO).

local equiv Game1_b_Game1_h:
  Game1(CR_b, Bt.RO, Rt.RO, Ht.LRO, Gt.LRO).run ~ Game1(CR_h, Bt.RO, Rt.RO, Ht.RO, Gt.LRO).run:
    ={glob A} ==> ={res, glob Game1}.
proof.
transitivity Game1(CR_h, Bt.RO, Rt.RO, Ht.LRO, Gt.LRO).run
  (={glob A} ==> ={res, glob Game1})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Gt.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Gt.RO.m{2} Rt.RO.m{2}.
+ proc.
  call (: ={glob Bt.RO}); 1:by sim.
  call (: ={glob Gt.RO}); 1:by sim.
  call (: ={glob Ht.RO}); 1:by sim.
  wp; call (: ={glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO, Game1.sk, Game1.qs}).
  + proc; inline {1} 1; inline {2} 1; inline {1} 3; inline {1} 2; inline {2} 4.
    call (: ={glob Ht.RO}); 1:by sim.
    wp; call (: ={glob Rt.RO}); 1:by sim.
    wp; call (: ={glob Bt.RO}); 1:by sim.
    by auto.
  + proc; inline {1} 1; inline {2} 1; inline {1} 3; inline {1} 2; inline {2} 4.
    call (: ={glob Gt.RO}); 1:by sim.
    wp; call (: ={glob Rt.RO}); 1:by sim.
    wp; call (: ={glob Bt.RO}); 1:by sim.
    by auto.
  + proc; inline {1} 1; inline {2} 1; inline {1} 3; inline {1} 2; inline {2} 4.
    sim; call (: ={glob Rt.RO}); 1:by sim.
    wp; call (: ={glob Bt.RO}); 1:by sim.
    by auto.
  by sim: (={glob A, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO, Game1.sk, Game1.qs, pk}).
transitivity Ht.MainD(Game1_h, Ht.LRO).distinguish
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Gt.RO} ==> ={res, glob Game1})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Gt.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Gt.RO.m{2} Rt.RO.m{2}.
+ proc *; inline {2} 1; wp.
  call (: ={glob A} ==> ={res, glob Game1}); first by sim.
  by inline *; auto.
transitivity Ht.MainD(Game1_h, Ht.RO).distinguish
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Gt.RO} ==> ={res, glob Game1})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Gt.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Gt.RO.m{2} Rt.RO.m{2}.
+ symmetry; conseq (Ht.FullEager.RO_LRO Game1_h _)=> />.
  by rewrite dgp_ll.
proc *; inline {1} 1; wp.
call (: ={glob A} ==> ={res, glob Game1}); first by sim.
by inline *; auto.
qed.

(** Sub-step 3: anticipate sampling of c **)
local module CR_g (B : Bt.RO) (R : Rt.RO) (H : Ht.RO) (G : Gt.RO) = {
  proc compute_related(m) = {
    var b, r, h;

    b <@ B.get(m);
    r <@ R.get(m);
    h <@ H.get(m, b);
         G.sample(h, h ^ Game1.sk, gg ^ r, h ^ r, m);
  }
}.

local module Game1_g (G : Gt.RO) = Game1(CR_g, Bt.RO, Rt.RO, Ht.RO, G).

local equiv Game1_h_Game1_g:
  Game1(CR_h, Bt.RO, Rt.RO, Ht.RO, Gt.LRO).run ~ Game1(CR_g, Bt.RO, Rt.RO, Ht.RO, Gt.RO).run:
    ={glob A} ==> ={res, glob Game1}.
proof.
transitivity Game1(CR_g, Bt.RO, Rt.RO, Ht.RO, Gt.LRO).run
  (={glob A} ==> ={res, glob Game1})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Ht.RO.m{2} Rt.RO.m{2}.
+ proc.
  call (: ={glob Bt.RO}); 1:by sim.
  call (: ={glob Gt.RO}); 1:by sim.
  call (: ={glob Ht.RO}); 1:by sim.
  wp; call (: ={glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO, Game1.sk, Game1.qs}).
  + proc; inline {1} 1; inline {2} 1; inline {1} 4; inline {2} 5.
    call (: ={glob Ht.RO}); 1:by sim.
    wp; call (: ={glob Ht.RO}); 1:by sim.
    wp; call (: ={glob Rt.RO}); 1:by sim.
    wp; call (: ={glob Bt.RO}); 1:by sim.
    by auto.
  + proc; inline {1} 1; inline {2} 1; inline {1} 4; inline {2} 5.
    call (: ={glob Gt.RO}); 1:by sim.
    wp; call (: ={glob Ht.RO}); 1:by sim.
    wp; call (: ={glob Rt.RO}); 1:by sim.
    wp; call (: ={glob Bt.RO}); 1:by sim.
    by auto.
  + proc; inline {1} 1; inline {2} 1; inline {1} 4; inline {2} 5.
    sim; call (: ={glob Ht.RO}); 1:by sim.
    by wp; sim: (={glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO, Game1.sk, Game1.qs, m, m0, b0}).
  by sim: (={glob A, glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO, Game1.sk, Game1.qs, pk}).
transitivity Gt.MainD(Game1_g, Gt.LRO).distinguish
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO} ==> ={res, glob Game1})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Ht.RO.m{2} Rt.RO.m{2}.
+ proc *; inline {2} 1; wp.
  call (: ={glob A} ==> ={res, glob Game1}); first by sim.
  by inline *; auto.
transitivity Gt.MainD(Game1_g, Gt.RO).distinguish
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO} ==> ={res, glob Game1})
  (={glob Game1, glob Bt.RO, glob Rt.RO, glob Ht.RO} ==> ={res, glob Game1})=> />.
+ by move=> &2; exists (glob A){2} Game1.b{2} Game1.bm{2} Game1.c{2} Game1.h{2}
                       Game1.m{2} Game1.qs{2} Game1.s{2} Game1.sk{2} Game1.y{2}
                       Bt.RO.m{2} Ht.RO.m{2} Rt.RO.m{2}.
+ symmetry; conseq (Gt.FullEager.RO_LRO Game1_g _)=> />.
  by rewrite dZp_ll.
proc *; inline {1} 1; wp.
call (: ={glob A} ==> ={res, glob Game1}); first by sim.
by inline *; auto.
qed.

module type Oracles = {
  proc init(): unit

  proc h(m : msg, b : bool): group
  proc g(h y a b : group, m : msg): zmod
  proc compute_related(m : msg): unit
}.

local module Shim (O : Oracles) = {
  var sk : zmod
  var qs : msg fset
  var ms : (msg, (group * (zmod * zmod) * bool)) fmap

  var  m : msg
  var  y : group
  var  c : zmod
  var  s : zmod
  var  b : bool
  var  h : group
  var bm : bool

  var gb : group

  module O' = {
    proc h(m, b) = {
      var r;

           O.compute_related(m);
      r <@ O.h(m, b);
      return r;
    }

    proc g(h, y, a, b, m) = {
      var r;

           O.compute_related(m);
      r <@ O.g(h, y, a, b, m);
      return r;
    }

    proc sign(m) = {
            O.compute_related(m);
      qs <- qs `|` fset1 m;
      return oget ms.[m];
    }
  }

  proc run() = {
    var pk, sig, pi, c';

                  O.init();
                  Bt.RO.init();
                  Rt.RO.init();
            qs <- fset0;
            ms <- empty;
            gb <$ dgp;

            sk <$ dZp;
            pk <- gg ^ sk;
      (m, sig) <@ A(O').forge(pk);

    (y, pi, b) <- sig;
        (c, s) <- pi;
             h <@ O.h(m, b);
            c' <@ O.g(h, y, gg ^ s * pk ^ -c, h ^ s * y ^ -c, m);
            bm <@ Bt.RO.get(m);

    return c = c' /\ !m \in qs
        /\ y = h ^ sk /\ bm <> b;
  }

  proc distinguish = run (* Fit the RO distinguisher interface *)
}.

local module O0 = {
  proc init() = {
    Ht.RO.init();
    Gt.RO.init();
  }

  proc h = Ht.RO.get
  proc g = Gt.RO.get

  proc compute_related(m) = {
    var b, r, h, c;

    if (m \notin Shim.ms) {
                 b <@ Bt.RO.get(m);
                 r <@ Rt.RO.get(m);
                 h <@ Ht.RO.get(m, b);
                 c <@ Gt.RO.get(h, h ^ Shim.sk, gg ^ r, h ^ r, m);
      Shim.ms.[m] <- (h ^ Shim.sk, (c, c * Shim.sk + r), b);
    }
  }
}.

local hoare Step1_b_hoare x0: Bt.RO.get:
     x = x0
  /\ (forall m y c s b,
           Shim.ms.[m] = Some (y, (c, s), b)
        => exists h,
                y = h ^ Shim.sk
             /\ Bt.RO.m.[m] = Some b
             /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
             /\ Ht.RO.m.[m, b] = Some h
             /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c)
  ==>    Bt.RO.m.[x0] = Some res
      /\ (forall m y c s b,
                Shim.ms.[m] = Some (y, (c, s), b)
             => exists h,
                     y = h ^ Shim.sk
                  /\ Bt.RO.m.[m] = Some b
                  /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
                  /\ Ht.RO.m.[m, b] = Some h
                  /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c).
proof.
proc; auto=> /> &0 ih b _; rewrite !domE=> /=.
rewrite get_set_sameE /=.
case _: (Bt.RO.m.[x0]{0})=> /> h_x m y c s b0 /ih /> {y} h0 b_m r_m h_mb g_hyabm.
by exists h0=> //=; rewrite get_set_neqE /#.
qed.

local equiv Step1_b x0: Bt.RO.get ~ Bt.RO.get:
     ={glob Bt.RO, x}
  /\ x{2} = x0
  /\ (forall m y c s b,
           Shim.ms.[m] = Some (y, (c, s), b)
        => exists h,
                y = h ^ Shim.sk
             /\ Bt.RO.m.[m] = Some b
             /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
             /\ Ht.RO.m.[m, b] = Some h
             /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}
  ==>    ={glob Bt.RO, res}
      /\ Bt.RO.m{2}.[x0] = Some res{2}
      /\ (forall m y c s b,
               Shim.ms.[m] = Some (y, (c, s), b)
            => exists h,
                    y = h ^ Shim.sk
                 /\ Bt.RO.m.[m] = Some b
                 /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
                 /\ Ht.RO.m.[m, b] = Some h
                 /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}.
proof.
by conseq (: ={glob Bt.RO, res}) _ (Step1_b_hoare x0); 3:by sim.
qed.

local hoare Step1_r_hoare x0: Rt.RO.get:
     x = x0
  /\ (forall m y c s b,
           Shim.ms.[m] = Some (y, (c, s), b)
        => exists h,
                y = h ^ Shim.sk
             /\ Bt.RO.m.[m] = Some b
             /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
             /\ Ht.RO.m.[m, b] = Some h
             /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c)
  ==>    Rt.RO.m.[x0] = Some res
      /\ (forall m y c s b,
                Shim.ms.[m] = Some (y, (c, s), b)
             => exists h,
                     y = h ^ Shim.sk
                  /\ Bt.RO.m.[m] = Some b
                  /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
                  /\ Ht.RO.m.[m, b] = Some h
                  /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c).
proof.
proc; auto=> /> &0 ih b _; rewrite !domE=> /=.
rewrite get_set_sameE /=.
case _: (Rt.RO.m.[x0]{0})=> /> h_x m y c s b0 /ih /> {y} h0 b_m r_m h_mb g_hyabm.
by exists h0=> //=; rewrite get_set_neqE /#.
qed.

local equiv Step1_r x0: Rt.RO.get ~ Rt.RO.get:
     ={glob Rt.RO, x}
  /\ x{2} = x0
  /\ (forall m y c s b,
           Shim.ms.[m] = Some (y, (c, s), b)
        => exists h,
                y = h ^ Shim.sk
             /\ Bt.RO.m.[m] = Some b
             /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
             /\ Ht.RO.m.[m, b] = Some h
             /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}
  ==>    ={glob Rt.RO, res}
      /\ Rt.RO.m{2}.[x0] = Some res{2}
      /\ (forall m y c s b,
               Shim.ms.[m] = Some (y, (c, s), b)
            => exists h,
                    y = h ^ Shim.sk
                 /\ Bt.RO.m.[m] = Some b
                 /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
                 /\ Ht.RO.m.[m, b] = Some h
                 /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}.
proof.
by conseq (: ={glob Rt.RO, res}) _ (Step1_r_hoare x0); 3:by sim.
qed.

local hoare Step1_h_hoare x0: Ht.RO.get:
     x = x0
  /\ (forall m y c s b,
           Shim.ms.[m] = Some (y, (c, s), b)
        => exists h,
                y = h ^ Shim.sk
             /\ Bt.RO.m.[m] = Some b
             /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
             /\ Ht.RO.m.[m, b] = Some h
             /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c)
  ==>    Ht.RO.m.[x0] = Some res
      /\ (forall m y c s b,
               Shim.ms.[m] = Some (y, (c, s), b)
            => exists h,
                    y = h ^ Shim.sk
                 /\ Bt.RO.m.[m] = Some b
                 /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
                 /\ Ht.RO.m.[m, b] = Some h
                 /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c).
proof.
proc; auto=> /> &0 ih h _; rewrite !domE=> /=.
rewrite get_set_sameE /=.
case _: (Ht.RO.m.[x0]{0})=> /> h_x m y c s b /ih /> {y} h0 b_m r_m h_mb g_yhabm.
by exists h0=> //=; rewrite g_yhabm /= get_set_neqE /#.
qed.

local equiv Step1_h x0: Ht.RO.get ~ Ht.RO.get:
     ={glob Ht.RO, x}
  /\ x{2} = x0
  /\ (forall m y c s b,
           Shim.ms.[m] = Some (y, (c, s), b)
        => exists h,
                y = h ^ Shim.sk
             /\ Bt.RO.m.[m] = Some b
             /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
             /\ Ht.RO.m.[m, b] = Some h
             /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}
  ==>    ={glob Ht.RO, res}
      /\ Ht.RO.m{2}.[x0] = Some res{2}
      /\ (forall m y c s b,
               Shim.ms.[m] = Some (y, (c, s), b)
            => exists h,
                    y = h ^ Shim.sk
                 /\ Bt.RO.m.[m] = Some b
                 /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
                 /\ Ht.RO.m.[m, b] = Some h
                 /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}.
proof.
by conseq (: ={glob Ht.RO, res}) _ (Step1_h_hoare x0); 3:by sim.
qed.

local hoare Step1_g_hoare x0: Gt.RO.get:
     x = x0
  /\ (forall m y c s b,
           Shim.ms.[m] = Some (y, (c, s), b)
        => exists h,
                y = h ^ Shim.sk
             /\ Bt.RO.m.[m] = Some b
             /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
             /\ Ht.RO.m.[m, b] = Some h
             /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c)
  ==>    Gt.RO.m.[x0] = Some res
      /\ (forall m y c s b,
               Shim.ms.[m] = Some (y, (c, s), b)
            => exists h,
                    y = h ^ Shim.sk
                 /\ Bt.RO.m.[m] = Some b
                 /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
                 /\ Ht.RO.m.[m, b] = Some h
                 /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c).
proof.
proc; auto=> /> &0 ih h _; rewrite !domE=> /=.
rewrite get_set_sameE /=.
case _: (Gt.RO.m.[x0]{0})=> /> h_x m y c s b /ih /> {y} h0 b_m r_m h_mb g_yhabm.
by exists h0=> //=; rewrite h_mb /= get_set_neqE /#.
qed.

local equiv Step1_g x0: Gt.RO.get ~ Gt.RO.get:
     ={glob Gt.RO, x}
  /\ x{1} = x0
  /\ (forall m y c s b,
           Shim.ms.[m] = Some (y, (c, s), b)
        => exists h,
                y = h ^ Shim.sk
             /\ Bt.RO.m.[m] = Some b
             /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
             /\ Ht.RO.m.[m, b] = Some h
             /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}
  ==>    ={glob Gt.RO, res}
      /\ Gt.RO.m{1}.[x0] = Some res{1}
      /\ (forall m y c s b,
               Shim.ms.[m] = Some (y, (c, s), b)
            => exists h,
                    y = h ^ Shim.sk
                 /\ Bt.RO.m.[m] = Some b
                 /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
                 /\ Ht.RO.m.[m, b] = Some h
                 /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}.
proof.
by conseq (: ={glob Gt.RO, res}) _ (Step1_g_hoare x0); 3:by sim.
qed.

local equiv Step1_cr m0: CR_g(Bt.RO, Rt.RO, Ht.RO, Gt.RO).compute_related ~ O0.compute_related:
     ={glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO, m}
  /\ ={sk}(Game1, Shim)
  /\ m{1} = m0
  /\ (forall m y c s b,
           Shim.ms.[m] = Some (y, (c, s), b)
        => exists h,
                y = h ^ Shim.sk
             /\ Bt.RO.m.[m] = Some b
             /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
             /\ Ht.RO.m.[m, b] = Some h
             /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}
  ==>    ={glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO}
      /\ (forall m y c s b,
               Shim.ms.[m] = Some (y, (c, s), b)
            => exists h,
                    y = h ^ Shim.sk
                 /\ Bt.RO.m.[m] = Some b
                 /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
                 /\ Ht.RO.m.[m, b] = Some h
                 /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}.
proof.
proc; if {2}.
+ inline {1} 4.
  wp; ecall (Step1_g (h, h ^ Game1.sk, gg ^ r, h ^ r, m){1}).
  wp; ecall (Step1_h (m, b){1}).
  wp; ecall (Step1_r m{1}).
  wp; ecall (Step1_b m{1}).
  auto=> /> &2 fun_rel; rewrite !domE=> /= m0_notin_ms.
  move=> b bM b_m0 Hb r rM r_m0 Hr h hM h_mb Hh c gM g_hyabm Hg.
  move=> m' y' c' s' b'; rewrite get_setE.
  case: (m' = m0)=> />; 2:by move=> _; exact: Hg.
  exists h=> //=; rewrite b_m0 h_mb.
  have ->: c' * Shim.sk{2} + r - c' * Shim.sk{2} = r by algebra.
  by rewrite g_hyabm.
inline *.
rcondf {1}  3.
+ auto=> /> fun_rel + _ _; rewrite !domE.
  by case _: (Shim.ms.[m]{m})=> /> [] y [] c s b /fun_rel /#.
rcondf {1}  6.
+ auto=> /> fun_rel + _ _ _ _; rewrite !domE.
  by case _: (Shim.ms.[m]{m})=> /> [] y [] c s b /fun_rel /#.
rcondf {1}  9.
+ auto=> /> fun_rel + _ _ _ _ _ _; rewrite !domE.
  by case _: (Shim.ms.[m]{m})=> /> [] y [] c s b /fun_rel /#.
rcondf {1} 13.
+ auto=> /> fun_rel + _ _ _ _ _ _; rewrite !domE.
  by case _: (Shim.ms.[m]{m})=> /> [] y [] c s b /fun_rel /#.
by auto=> />; rewrite dgp_ll.
qed.

local module DistH (B : Bt.RO) (G : Gt.RO) (H : Ht.RO) = Game0(B, H, G).

local lemma pr_Game0_Shim &m:
    Pr[Game0(Bt.RO, Ht.RO, Gt.RO).run() @ &m:
            res
         /\ Game0.y = Game0.h ^ Game0.sk
         /\ Bt.RO.m.[Game0.m] <> Some Game0.b]
  = Pr[Shim(O0).run() @ &m: res].
proof.
have ->: Pr[Game0(Bt.RO, Ht.RO, Gt.RO).run() @ &m:
                 res
              /\ Game0.y = Game0.h ^ Game0.sk
              /\ Bt.RO.m.[Game0.m] <> Some Game0.b]
       = Pr[Game0(Bt.RO, Ht.LRO, Gt.RO).run() @ &m:
                 res
              /\ Game0.y = Game0.h ^ Game0.sk
              /\ Bt.RO.m.[Game0.m] <> Some Game0.b].
+ byequiv (Ht.FullEager.RO_LRO_D (DistH(Bt.RO, Gt.RO)) _)=> //.
  by rewrite dgp_ll.
have ->: Pr[Game0(Bt.RO, Ht.LRO, Gt.RO).run() @ &m:
                 res
              /\ Game0.y = Game0.h ^ Game0.sk
              /\ Bt.RO.m.[Game0.m] <> Some Game0.b]
       = Pr[Game0(Bt.RO, Ht.LRO, Gt.LRO).run() @ &m:
                 res
              /\ Game0.y = Game0.h ^ Game0.sk
              /\ Bt.RO.m.[Game0.m] <> Some Game0.b].
+ byequiv (Gt.FullEager.RO_LRO_D (Game0(Bt.RO, Ht.LRO)) _)=> //.
  by rewrite dZp_ll.
have ->: Pr[Game0(Bt.RO, Ht.LRO, Gt.LRO).run() @ &m:
                 res
              /\ Game0.y = Game0.h ^ Game0.sk
              /\ Bt.RO.m.[Game0.m] <> Some Game0.b]
       = Pr[Game1(CR_b, Bt.RO, Rt.RO, Ht.LRO, Gt.LRO).run() @ &m:
                 res
              /\ Game1.y = Game1.h ^ Game1.sk
              /\ Game1.bm <> Game1.b].
+ by byequiv (Game0_Game1_b)=> /> &1 &2 ->.
have ->: Pr[Game1(CR_b, Bt.RO, Rt.RO, Ht.LRO, Gt.LRO).run() @ &m:
                 res
              /\ Game1.y = Game1.h ^ Game1.sk
              /\ Game1.bm <> Game1.b]
       = Pr[Game1(CR_h, Bt.RO, Rt.RO, Ht.RO, Gt.LRO).run() @ &m:
                 res
              /\ Game1.y = Game1.h ^ Game1.sk
              /\ Game1.bm <> Game1.b].
+ by byequiv (Game1_b_Game1_h).
have ->: Pr[Game1(CR_h, Bt.RO, Rt.RO, Ht.RO, Gt.LRO).run() @ &m:
                 res
              /\ Game1.y = Game1.h ^ Game1.sk
              /\ Game1.bm <> Game1.b]
       = Pr[Game1(CR_g, Bt.RO, Rt.RO, Ht.RO, Gt.RO).run() @ &m:
                 res
              /\ Game1.y = Game1.h ^ Game1.sk
              /\ Game1.bm <> Game1.b].
+ by byequiv (Game1_h_Game1_g).
byequiv=> //; proc.
sim: (   ={glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO, c'}
      /\ ={qs, sk, m, y, b, c, s, h, bm}(Game1, Shim))=> />.
wp.
call (:    ={glob Bt.RO, glob Rt.RO, glob Ht.RO, glob Gt.RO}
        /\ ={qs, sk}(Game1, Shim)
        /\ (forall m y c s b,
                 Shim.ms.[m] = Some (y, (c, s), b)
              => exists h,
                      y = h ^ Shim.sk
                   /\ Bt.RO.m.[m] = Some b
                   /\ Rt.RO.m.[m] = Some (s - c * Shim.sk)
                   /\ Ht.RO.m.[m, b] = Some h
                   /\ Gt.RO.m.[h, y, gg ^ (s - c * Shim.sk), h ^ (s - c * Shim.sk), m] = Some c){2}).
+ by proc; ecall (Step1_h (m, b){1}); ecall (Step1_cr m{1}); auto.
+ by proc; ecall (Step1_g (h, y, a, b, m){1}); ecall (Step1_cr m{1}); auto.
+ proc; inline {1} 1; inline {2} 1.
  case: ((m \in Shim.ms){2}).
  + rcondf {2}  2; first by auto.
    inline {1}  2; rcondf {1}  4.
    + auto=> /> fun_rel + _ _; rewrite !domE.
      by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h ->.
    inline {1}  5; rcondf {1}  7.
    + auto=> /> fun_rel + _ _ _ _; rewrite !domE.
      by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h _ ->.
    inline {1}  8; rcondf {1} 10.
    + auto=> /> fun_rel + _ _ _ _; rewrite !domE.
      by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h -> _ ->.
    inline {1} 11; inline {1} 12; rcondf {1} 14.
    + auto=> /> fun_rel + _ _ _ _ _ _; rewrite !domE.
      by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h -> -> -> ->.
    inline {1} 15; rcondf {1} 17.
    + auto=> /> fun_rel + _ _; rewrite !domE.
      by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h ->.
    inline {1} 18; rcondf {1} 20.
    + auto=> /> fun_rel + _ _ _ _; rewrite !domE.
      by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h _ ->.
    inline {1} 21; rcondf {1} 23.
    + auto=> /> fun_rel + _ _ _ _ _ _; rewrite !domE.
      by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h -> _ ->.
    inline {1} 24; rcondf {1} 26.
    + auto=> /> fun_rel + _ _ _ _ _ _; rewrite !domE.
      by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h -> -> -> ->.
    auto=> /> &2 fun_rel; rewrite dgp_ll=> /= + _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _; rewrite !domE.
    case _: (Shim.ms.[m]{2})=> /> [] y [] c s b /fun_rel /> {y} h -> -> -> -> /=.
    by algebra.
  rcondt {2}  2; first by auto.
  seq  6  7: (#[1]pre /\ ={m0, m} /\ (m \in Shim.ms){2}).
  + inline {1} 5; wp; ecall (Step1_g (h, h ^ Shim.sk, gg ^ r, h ^ r, m0){2}).
    wp; ecall (Step1_h (m0, b){2}).
    ecall (Step1_r m0{2}).
    ecall (Step1_b m0{2}).
    auto=> /> &2 fun_rel; rewrite !domE=> /= m_notin_ms.
    move=> b bM b_m _ r rM r_m _ h hM h_mb _ c gM g_hyabm fun_rel'.
    rewrite mem_set=> /= m' y' c' s' b'; rewrite get_setE.
    case: (m' = m{2})=> />; 2:by move=> _; exact: fun_rel'.
    by exists h; have ->: c' * Shim.sk{2} + r - c' * Shim.sk{2} = r by algebra.
  inline *; rcondf {1} 3.
  + auto=> /> fun_rel + _ _; rewrite !domE.
    by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h' ->.
  rcondf {1}  6.
  + auto=> /> fun_rel + _ _ _ _; rewrite !domE.
    by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h' _ ->.
  rcondf {1}  9.
  + auto=> /> fun_rel + _ _ _ _; rewrite !domE.
    by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h' -> _ ->.
  rcondf {1} 12.
  + auto=> /> fun_rel + _ _ _ _ _ _; rewrite !domE.
    by case _: (Shim.ms.[m]{m0})=> /> [] y [] c s b /fun_rel /> {y} h' -> -> -> ->.
  auto=> /> &2 fun_rel; rewrite !domE dgp_ll=> /= + _ _ _ _ _ _ _ _.
  case _: (Shim.ms.[m]{2})=> /> [] y [] c s b /fun_rel /> {y} h' -> -> -> -> /=.
  by algebra.
by inline *; auto=> />; smt(dgp_ll emptyE).
qed.

(* We do both embedding and simulation in one step *)
local module O1 = {
  var hm : (msg * bool, h_resp) fmap
  var gm : (group * group * group * group * msg, zmod) fmap

  proc init() = {
    hm <- empty;
    gm <- empty;
          Gt.RO.init();
  }

  proc h(x) = {
    var y;

    y <$ dZp;
    if (x \notin hm) {
      hm.[x] <- AHQuery y;
    }
    return get_h Shim.gb (oget hm.[x]);
  }

  proc g(x) = {
    var y;

    y <$ dZp;
    if (x \notin gm) {
      gm.[x] <- y;
    }
    return oget gm.[x];
  }

  proc compute_related(m) = {
    var b, c, d, h, s, pk, y, u, v;

    if (m \notin Shim.ms) {
                         b <@ Bt.RO.get(m);
                         s <@ Rt.RO.get(m);
                         d <$ dZp;
               hm.[(m, b)] <- SHQuery d;
                         h <- gg ^ d;

                         c <$ dZp;
                        pk <- gg ^ Shim.sk;
                         y <- pk ^ d;
                         u <- gg ^ s * pk ^ -c;
                         v <- h ^ s * y ^ -c;
      gm.[(h, y, u, v, m)] <- c;
       
               Shim.ms.[m] <- (y, (c, s), b);
    }
  }
}.

local equiv Shim0_Shim1_H hm0 m0 b0: Ht.RO.get ~ O1.h:
     ={Shim.gb, arg}
  /\ arg{1} = (m0, b0)
  /\ Ht.RO.m{1} = hm0
  /\ Ht.RO.m{1} = (map (fun _=> get_h Shim.gb) O1.hm){2}
  ==>    ={Shim.gb, res}
      /\ (forall m b, (m, b) \in Ht.RO.m{1} <=> (m, b) \in hm0 \/ (m, b) = (m0, b0)) 
      /\ Ht.RO.m{1} = (map (fun _=> get_h Shim.gb) O1.hm){2}.
proof.
proc.
wp; rnd (fun h=> logge (h / Shim.gb){2}) (fun (d : zmod)=> Shim.gb{2} * gg ^ d).
auto=> /> &2; split=> [|_].
+ by move=> d _; rewrite !logDr logrV loggK #ring.
split=> [|_].
+ by move=> d _; rewrite dZp1E cardE dgp1E.
move=> h _; split=> [|_].
+ by rewrite logDr expD !expgK CyclicGroup.mulcC -CyclicGroup.mulcA CyclicGroup.mulVc CyclicGroup.mulc1.
rewrite mem_map !domE; case _: (O1.hm.[m0, b0]{2})=> /> => [_|d hm_x].
+ rewrite !get_set_sameE /=.
  rewrite logDr expD !expgK CyclicGroup.mulcC -CyclicGroup.mulcA CyclicGroup.mulVc CyclicGroup.mulc1 /=.
  split.
  + by move=> b m; rewrite !(mem_map, mem_set).
  apply: fmap_eqP=> x0; rewrite !get_setE; case: (x0 = (m0, b0))=> [/>|neq_x].
  + rewrite mapE get_set_sameE /=.
    by rewrite expD !expgK CyclicGroup.mulcC -CyclicGroup.mulcA CyclicGroup.mulVc CyclicGroup.mulc1.
  by rewrite !mapE get_set_neqE.
rewrite mapE hm_x=> /= m b; rewrite !mem_map=> - [] />.
by rewrite domE hm_x.
qed.

local equiv Shim0_Shim1_G gm0 h0 y0 u0 v0 m0: Gt.RO.get ~ O1.g:
     ={arg}
  /\ arg{1} = (h0, y0, u0, v0, m0)
  /\ Gt.RO.m{1} = gm0
  /\ Gt.RO.m{1} = O1.gm{2}
  ==>    ={res}
      /\ (forall h y u v m, (h, y, u, v, m) \in Gt.RO.m{1} <=> (h, y, u, v, m) \in gm0 \/ (h, y, u, v, m) = (h0, y0, u0, v0, m0))
      /\ Gt.RO.m{1} = O1.gm{2}.
proof.
proc; auto=> /> c _; rewrite !domE.
case _: gm0.[h0, y0, u0, v0, m0]=> />.
+ by move=> + h y u v m; rewrite mem_set.
by move=> c0 + h y u v m [//|/>]; rewrite !domE=> ->.
qed.

local equiv Shim0_Shim1_CR ms0 m0: O0.compute_related ~ O1.compute_related:
     ={glob Bt.RO, Shim.ms, Shim.gb, Shim.sk, Shim.qs, arg}
  /\ arg{1} = m0
  /\ Shim.ms{1} = ms0
  /\ dom Rt.RO.m{1} = dom Rt.RO.m{2}
  /\ Ht.RO.m{1} = (map (fun _=> get_h Shim.gb) O1.hm){2}
  /\ Gt.RO.m{1} = O1.gm{2}
  /\ (forall m, m \notin Shim.ms => m \notin Bt.RO.m){2}
  /\ (forall m, m \notin Shim.ms => m \notin Rt.RO.m){2}
  /\ (forall m, m \notin Shim.ms => forall b, (m, b) \notin O1.hm){2}
  /\ (forall m, m \notin Shim.ms => forall h y u v, (h, y, u, v, m) \notin O1.gm){2}
  ==>    ={glob Bt.RO, Shim.ms, Shim.gb, Shim.sk, Shim.qs}
      /\ (forall m, m \in Shim.ms{1} <=> m \in ms0 \/ m = m0)
      /\ dom Rt.RO.m{1} = dom Rt.RO.m{2}
      /\ Ht.RO.m{1} = (map (fun _=> get_h Shim.gb) O1.hm){2}
      /\ Gt.RO.m{1} = O1.gm{2}
      /\ (forall m, m \notin Shim.ms => m \notin Bt.RO.m){2}
      /\ (forall m, m \notin Shim.ms => m \notin Rt.RO.m){2}
      /\ (forall m, m \notin Shim.ms => forall b, (m, b) \notin O1.hm){2}
      /\ (forall m, m \notin Shim.ms => forall h y u v, (h, y, u, v, m) \notin O1.gm){2}.
proof.
proc; if; 1:by auto.
+ inline {1}  1; rcondt {1}  3.
  + by auto=> /> &2 _ domb _ _ _ /domb ->.
  inline {2}  1; rcondt {2}  3.
  + by auto=> /> &2 _ domb _ _ _ /domb ->.
  inline {1}  5; rcondt {1}  7.
  + by auto=> /> &2 -> _ domr _ _ /domr ->.
  inline {2}  5; rcondt {2}  7.
  + by auto=> /> &2 _ _ domr _ _ /domr ->.
  inline {1}  9; rcondt {1} 11.
  + auto=> /> &2 _ _ _ domh _ /domh + b _ _ _ _ _.
    by rewrite get_set_sameE mem_map=> ->.
  inline {1} 13; rcondt {1} 15.
  + auto=> /> &2 _ _ _ _ domg /domg + b _ r _ h _ _ _.
    by rewrite !get_set_sameE=> ->.
  swap {1} 14 -13; swap {2} 12 -11.
  wp; rnd logge (fun (d : zmod)=> gg ^ d).
  wp; rnd (fun s=> c{2} * Shim.sk{2} + s) (fun r=> r - c{2} * Shim.sk{2}).
  auto=> /> &1 &2 eq_domr domb domr domh domg m_notin_ms.
  move=> c _ b _; split=> [r _|_]; 1:by algebra.
  move=> s _; split=> [|_]; 1:by algebra.
  split=> [d _|_]; 1:by rewrite loggK.
  split=> [d _|_]; 1:by rewrite dZp1E dgp1E cardE.
  move=> h _; split=> [|_]; 1:by rewrite expgK.
  rewrite !get_set_sameE /=.
  rewrite -expM (ZModpField.mulrC Shim.sk{2}) expM !expgK -!expM -!expD.
  have -> //=: c * Shim.sk{2} + s + Shim.sk{2} * -c = s by algebra.
  split=> [m|]; 1:by rewrite mem_set.
  split.
  + by apply: fun_ext=> x; rewrite !mem_set eq_domr.
  split.
  + apply: fmap_eqP=> - [] m' b'; rewrite !(mapE, get_setE).
    by case: ((m', b') = (m0, b))=> />; rewrite expgK.
  split=> [m|].
  + by rewrite !mem_set !negb_or=> /> /domb.
  split=> [m|].
  + by rewrite !mem_set !negb_or=> /> /domr.
  split=> [m + b'|].
  + by rewrite !mem_set !negb_or=> /> /domh ->.
  move=> m + h' y' u' v'.
  by rewrite !mem_set !negb_or=> /> /domg ->.
by auto=> /> _ _ _ _ _ _ _ + m1 [].
qed.

local lemma pr_Shim0_Shim1 &m:
    Pr[Shim(O0).run() @ &m: res]
  = Pr[Shim(O1).run() @ &m: res].
proof.
byequiv (: ={glob A} ==> ={res})=> //=.
proc.
sim: (={Shim.c, c', Shim.m, Shim.qs, Shim.sk, Shim.y, Shim.h, Shim.bm, Shim.b})=> />.
ecall (Shim0_Shim1_H Ht.RO.m{1} Shim.m{1} Shim.b{1}).
wp.
call (:    ={glob Bt.RO, Shim.ms, Shim.gb, Shim.sk, Shim.qs}
        /\ dom Rt.RO.m{1} = dom Rt.RO.m{2}
        /\ Ht.RO.m{1} = (map (fun _=> get_h Shim.gb) O1.hm){2}
        /\ Gt.RO.m{1} = O1.gm{2}
        /\ (forall m, m \notin Shim.ms => m \notin Bt.RO.m){2}
        /\ (forall m, m \notin Shim.ms => m \notin Rt.RO.m){2}
        /\ (forall m, m \notin Shim.ms => forall b, (m, b) \notin O1.hm){2}
        /\ (forall m, m \notin Shim.ms => forall h y u v, (h, y, u, v, m) \notin O1.gm){2}).
+ proc=> /=.
  ecall (Shim0_Shim1_H  Ht.RO.m{1} m{1} b{1}).
  ecall (Shim0_Shim1_CR Shim.ms{1} m{1}).
  by auto; smt(mem_map).
+ proc=> /=.
  ecall (Shim0_Shim1_G Gt.RO.m{1} h{1} y{1} a{1} b{1} m{1}).
  ecall (Shim0_Shim1_CR Shim.ms{1} m{1}).
  by auto; smt(mem_map).
+ by proc; wp; ecall (Shim0_Shim1_CR Shim.ms{1} m{1}); auto.
by inline *; auto=> /> gb _ _ _; rewrite map_empty #smt:(emptyE).
qed.

local equiv Shim1_Simulator_CR ms0 m0: O1.compute_related ~ Simulator(A).compute_related:
     ={arg}
  /\ arg{1} = m0
  /\ Shim.ms{1} = ms0
  /\ Shim.ms{1} = Simulator.ms{2}
  /\ Shim.gb{1} = Simulator.gb{2}
  /\ gg ^ Shim.sk{1} = Simulator.pk{2}
  /\ O1.hm{1} = Simulator.hm{2}
  /\ O1.gm{1} = Simulator.gm{2}
  /\ (dom Shim.ms = dom Bt.RO.m){1}
  /\ (dom Shim.ms = dom Rt.RO.m){1}
  /\ (forall m, Shim.ms.[m] = None => forall b, O1.hm.[m, b] = None){1}
  /\ (forall m b h,
           O1.hm.[m, b] = Some h
        => Bt.RO.m.[m] <> Some b
        => exists d, h = AHQuery d){1}
  /\ (forall m y c s b,
           Shim.ms.[m] = Some (y, (c, s), b)
        => let r = s + Shim.sk * -c in
           exists (d : zmod),
                y = gg ^ d ^ Shim.sk
             /\ Bt.RO.m.[m] = Some b
             /\ O1.hm.[m, b] = Some (SHQuery d)
             /\ O1.gm.[gg ^ d, y, gg ^ r, gg ^ d ^ r, m] = Some c){1}
  ==>    (forall m, m \in Shim.ms <=> (m \in ms0 \/ m = m0)){1}
      /\ Shim.ms{1} = Simulator.ms{2}
      /\ Shim.gb{1} = Simulator.gb{2}
      /\ gg ^ Shim.sk{1} = Simulator.pk{2}
      /\ O1.hm{1} = Simulator.hm{2}
      /\ O1.gm{1} = Simulator.gm{2}
      /\ (dom Shim.ms = dom Bt.RO.m){1}
      /\ (dom Shim.ms = dom Rt.RO.m){1}
      /\ (forall m, Shim.ms.[m] = None => forall b, O1.hm.[m, b] = None){1}
      /\ (forall m b h,
               O1.hm.[m, b] = Some h
            => Bt.RO.m.[m] <> Some b
            => exists d, h = AHQuery d){1}
      /\ (forall m y c s b,
               Shim.ms.[m] = Some (y, (c, s), b)
            => let r = s + Shim.sk * -c in
               exists (d : zmod),
                    y = gg ^ d ^ Shim.sk
                 /\ Bt.RO.m.[m] = Some b
                 /\ O1.hm.[m, b] = Some (SHQuery d)
                 /\ O1.gm.[gg ^ d, y, gg ^ r, gg ^ d ^ r, m] = Some c){1}.
proof.
proc; if; [1:by auto|3:by auto=> /> &1 &2 -> /#].
inline {1} 1; rcondt {1} 3; first by auto=> /#.
inline {1} 5; rcondt {1} 7; first by auto=> /#.
auto=> /> &1 &2 dom_b dom_r dom_h aQ wf_sigs m_notin_ms.
move=> b _ s _ d _ c _; rewrite !get_set_sameE /= 2!fun_ext; split=> [m|].
+ by rewrite mem_set.
split=> [m|].
+ by rewrite !mem_set dom_b.
split=> [m|].
+ by rewrite !mem_set dom_r.
split=> [m + b0|].
+ by rewrite !get_setE=> /=; case: (m = m0)=> /> _ /dom_h ->.
split=> [m' b' h'|].
+ by rewrite !get_setE; case: ((m', b') = (m0, b))=> /#.
move=> m' y' c' s' b'; rewrite !get_setE; case: (m0 = m')=> />.
+ exists d=> //=.
  rewrite -expM ZModpField.mulrC expM //=.
  by rewrite -!expM -!expD ZModpField.mulrDr ZModpField.mulrA get_set_sameE.
rewrite eq_sym=> ^ neq_m -> /= msH.
move: (wf_sigs _ _ _ _ _ msH)=> /> d' ->> b_m' hm_mb' gm_hyabm'; exists d'=> //=.
by rewrite hm_mb' get_set_neqE /= 1:neq_m.
qed.

local lemma pr_Shim1_Simulator &m :
     Pr[Shim(O1).run() @ &m : res]
  <= Pr[CDH(Simulator(A)).run() @ &m : res].
proof.
byequiv (: ={glob A} ==> res{1} => res{2})=> //.
proc; inline Simulator(A).solve O1.init.
swap {1} 8 -7; swap {1} 9 -8.
seq 13 12: (   ={glob A}
            /\ Shim.m{1} = m{2}
            /\ Shim.y{1} = y{2}
            /\ Shim.b{1} = b0{2}
            /\ Shim.c{1} = c{2}
            /\ Shim.s{1} = s{2}
            /\ Shim.sk{1} = a{2}
            /\ gg ^ Shim.sk{1} = Simulator.pk{2}
            /\ Shim.gb{1} = gg ^ b{2}
            /\ O1.hm{1} = Simulator.hm{2}
            /\ O1.gm{1} = Simulator.gm{2}
            /\ (dom Shim.ms = dom Bt.RO.m){1}
            /\ (dom Shim.ms = dom Rt.RO.m){1}
            /\ (forall m, Shim.ms.[m] = None => forall b, O1.hm.[m, b] = None){1}
            /\ (forall m b h,
                     O1.hm.[m, b] = Some h
                  => Bt.RO.m.[m] <> Some b
                  => exists d, h = AHQuery d){1}
            /\ (forall m y c s b,
                     Shim.ms.[m] = Some (y, (c, s), b)
                  => let r = s + Shim.sk * -c in
                     exists (d : zmod),
                          y = gg ^ d ^ Shim.sk
                       /\ Bt.RO.m.[m] = Some b
                       /\ O1.hm.[m, b] = Some (SHQuery d)
                       /\ O1.gm.[gg ^ d, y, gg ^ r, gg ^ d ^ r, m] = Some c){1}).
+ wp.
  call (:    gg ^ Shim.sk{1} = Simulator.pk{2}
          /\ Shim.gb{1} = Simulator.gb{2}
          /\ Shim.ms{1} = Simulator.ms{2}
          /\ O1.hm{1} = Simulator.hm{2}
          /\ O1.gm{1} = Simulator.gm{2}
          /\ (dom Shim.ms = dom Bt.RO.m){1}
          /\ (dom Shim.ms = dom Rt.RO.m){1}
          /\ (forall m, Shim.ms.[m] = None => forall b, O1.hm.[m, b] = None){1}
          /\ (forall m b h,
                   O1.hm.[m, b] = Some h
                => Bt.RO.m.[m] <> Some b
                => exists d, h = AHQuery d){1}
          /\ (forall m y c s b,
                   Shim.ms.[m] = Some (y, (c, s), b)
                => let r = s + Shim.sk * -c in
                   exists (d : zmod),
                        y = gg ^ d ^ Shim.sk
                     /\ Bt.RO.m.[m] = Some b
                     /\ O1.hm.[m, b] = Some (SHQuery d)
                     /\ O1.gm.[gg ^ d, y, gg ^ r, gg ^ d ^ r, m] = Some c){1}).
  + proc; inline {1} 2; inline {2} 2.
    auto.
    ecall (Shim1_Simulator_CR Shim.ms{1} m{1}).
    auto=> /> &1 &2 dom_b dom_r dom_h aQ wf_sigs.
    move=> bM rM gM hM msM in_msM dom_bM dom_rM dom_hM aQ' wf_sigs'.
    move=> d _; rewrite domE=> /= hM_mb; split=> [m0 + b0|].
    + rewrite get_setE; case: ((m0, b0) = (m, b){2})=> />.
      + by rewrite -domE in_msM.
      by move=> _ /dom_hM ->.
    split=> [m' b' h'|m' y' c' s' b'].
    + by rewrite get_setE; case: ((m', b') = (m, b){2})=> /> _; [exists d|exact: aQ'].
    rewrite get_setE; case: ((m', b') = (m, b){2})=> />.
    + by rewrite -negP=> /wf_sigs' /#.
    by move=> _; exact: wf_sigs'.
  + proc; inline {1} 2; inline {2} 2.
    auto.
    ecall (Shim1_Simulator_CR Shim.ms{1} m{1}).
    auto=> /> &1 &2 dom_b dom_r dom_h aQ wf_sigs.
    move=> bM rM gM hM msM in_msM dom_bM dom_rM dom_hM aQ' wf_sigs'.
    move=> d _; rewrite domE=> /= hM_mb m' y' c' s' b'.
    move=> /wf_sigs' /> {y'} d' bM_m' hM_mb' gM_hyabm'; exists d'=> //=.
    by rewrite get_set_neqE 1:/# hM_mb' gM_hyabm'.
  + by proc; wp; ecall (Shim1_Simulator_CR Shim.ms{1} m{1}); auto.
  inline *; wp; rnd logge (fun (x : zmod)=> gg ^ x); auto=> /> sk _.
  split=> [b _|_]; 1:by rewrite loggK.
  split=> [b _|_]; 1:by rewrite dZp1E dgp1E cardE.
  by move=> gb _; rewrite expgK=> //=; smt(emptyE).
inline *; wp; rnd {1}; wp; rnd {1}; auto=> /> &1 &2.
move=> dom_b dom_r dom_h aQ wf_sigs d _; rewrite get_set_sameE !domE /=.
case _: (Simulator.hm.[m, b0]{2})=> //= />.
+ move=> hm_mb c' _; rewrite !get_set_sameE.
  have -> //: ((gg ^ b * gg ^ d) ^ a * gg ^ a ^ -d = gg ^ (a * b)){2}.
  by rewrite -!(expM, expD); congr; field.
move=> [] /= d' hm_mb c' _; rewrite !get_set_sameE /=.
+ have -> //: ((gg ^ b * gg ^ d') ^ a * gg ^ a ^ -d' = gg ^ (a * b)){2}.
  by rewrite -!(expM, expD); congr; field.
move: (dom_h m{2})=> /contra; rewrite negb_forall=> /(_ _).
+ by exists b0{2}; rewrite /= hm_mb.
case _: (Shim.ms{1}.[m{2}])=> /> [] h [] c s b ms_m.
move: (wf_sigs _ _ _ _ _ ms_m)=> /> d0 ->> b_m hm_mb' gm_hyabm.
move: (aQ _ _ _ hm_mb)=> /contra; rewrite negb_exists=> /(_ _) //=.
by rewrite b_m=> /= <<-; move: hm_mb'; rewrite hm_mb.
qed.

lemma Security &m:
     Pr[EUF_CMA(Ht.RO, Gt.RO, GJKW, A).run() @ &m : res]
  <=   (qG + 1)%r / p%r
     +   2%r
       * Pr[CDH(Simulator(A)).run() @ &m : res].
proof.
apply: (StdOrder.RealOrder.ler_trans _ (decompose &m)).
rewrite (pr_Game0_Shim &m) (pr_Shim0_Shim1 &m) ler_add2l.
by apply: ler_wpmul2l=> //; rewrite (pr_Shim1_Simulator &m).
qed.

end section.
print Security.
