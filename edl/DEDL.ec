(************* EDL scheme *******************************************)
require import AllCore Bool FMap Int IntDiv Real List FSet.
require import Distr DInterval DProd DBool Mu_mem.
(*---*) import StdOrder.RealOrder StdBigop.Bigreal.BRA.

require (*--*) FelTactic.
require (*--*) ROM ZModP.

pragma -oldip. pragma +implicits.

op q: {int | prime q} as prime_q.
clone export ZModP.ZModField as PFo with
  op p <- q
proof prime_p by exact/prime_q.
import ZModpField.

lemma ge2_q: 2 <= q.
proof. smt(gt1_prime prime_q). qed.

op dp = dapply inzmod [0..q-1].

lemma dp_ll: is_lossless dp.
proof. by apply/dmap_ll/dinter_ll; smt(ge2_q). qed. 

lemma dp_uni: is_uniform dp.
proof.
rewrite dmap_uni_in_inj 2:dinter_uni=> x y.
rewrite !supp_dinter=> x_bnd y_bnd.
rewrite -{2}(@modz_small x q) 2:-{2}(@modz_small y q) 1,2:#smt:(ge2_q).
by rewrite -(@inzmodK x) -(@inzmodK y)=> ->.
qed.

lemma dp_fu: is_full dp.
proof.
move=> z; rewrite supp_dmap; exists (asint z).
rewrite supp_dinter -(@ltzS (asint z)) Sub.valP /=.
by rewrite /inzmod modz_small 1:#smt:(Sub.valP ge2_q) Sub.valKd.
qed.

lemma dp1E z: mu1 dp z = 1%r / q%r.
proof.
rewrite dmap1E -(@mu_eq_support _ (pred1 (asint z))).
+ move=> x /supp_dinter x_bnd @/pred1 @/(\o); rewrite eq_iff.
  split=> [->>|<<-].
  + by rewrite /inzmod /asint modz_small 1:#smt:(ge2_q) Sub.valKd.
  by rewrite inzmodK modz_small 1:#smt:(ge2_q).
by rewrite dinter1E -(@ltzS (asint z)) Sub.valP.
qed.

hint exact: dp_ll dp_uni dp_fu dp1E.

(***** G_g,p  ****)
theory Group_GP.

type _GP.
op eg: _GP.
op ( * ): _GP -> _GP -> _GP.
op inv: _GP -> _GP.

clone include Ring.ZModule with
  type t        <- _GP,
  op   zeror    <- eg,
  op   (+)      <- ( * ),
  op   [-]      <- inv

rename "intmul" as "intpow"
rename "addr"   as "mulg1"
rename "add2r"  as "mul2g1"
rename "mulr"   as "powg1"
rename "add0r"  as "mulg1r"  (**  ( 0 * something) *)
rename "addr0"  as "mulrg1"
rename "addrA" as "mulg1g1g1"
rename "oppr0" as "oppr0".

(* a generator *)
op gg: _GP.
(* FIXME: this should be a consequence of the fact that gg is a generator *)
axiom g_n: gg <> eg.

(* distribution over Group (_GP) & named it dgp *)
(* FIXME: define and prove equivalence to "dapply (fun x=> gg ^ x) dp" *)
(*        or define as "dapply (fun x=> gg ^ x) dp" and prove full and uniform *)
op dgp: _GP distr.

axiom dgp_ll : is_lossless dgp.
axiom dgp1E g: mu1 dgp g = 1%r / q%r.

op (^) x z = intpow x (asint z).
(* FIXME: q is the order of the group *)
axiom intpow_mod x z: intpow x z = intpow x (z %% q).

(* FIXME: figure out if we can define log constructively rather than axiomatically *)
op log: _GP -> _GP ->  zmod.
axiom logK (g: _GP) (z: zmod): g <> eg => log g (g^z) = z.
axiom powK (g: _GP) (x: _GP): g <> eg => g ^ (log g x ) = x.

lemma expgS x n: intpow x (n + 1) = x * intpow x n.
proof.
case: (0 <= n); first exact powg1S.
rewrite -ltzNge -oppz_gt0 => ge0_Nn.
rewrite -oppzK powg1Nz -mulNrz.
move: (powg1S (inv x) (-n - 1) _).
+ smt().
rewrite mulNrz -powg1Nz=> ->.
by rewrite mulg1A subrr mulg1r /#.
qed.

lemma expDnz g m n:
  0 <= m =>
  intpow g m * intpow g n = intpow g (m + n).
proof.
elim: m.
+ rewrite powg10z; exact/mulg1r.
by move=> i ge0_i ih; rewrite addzAC !expgS -ih AddMonoid.addmA.
qed.

lemma expDz g m n: intpow g m * intpow g n = intpow g (m + n).
proof.
case: (0 <= m); first by move/expDnz; exact.
rewrite -{1}(@oppzK m) oppz_ge0 -ltzNge=> /ltzW ge0_Nm.
rewrite -(@oppzK (m + n)) powg1Nz Ring.IntID.opprD.
by rewrite -(@expDnz g (-m)) // opprD -!powg1Nz !oppzK.
qed.

lemma expMnz g m n:
  0 <= n =>
  intpow (intpow g m) n = intpow g (m * n).
proof.
elim: n=> //=; first by rewrite !powg10z.
by move=> n ge0_n ih; rewrite expgS ih mulzDr expDz mulz1 mulzC addzC.
qed.

lemma expMz g m n: intpow (intpow g m) n = intpow g (m * n).
proof.
case (0<= n); first by move=> /expMnz; exact.
rewrite -(@oppzK n) oppz_ge0 -ltzNge=> /ltzW.
by move=> ge0_Nm; rewrite powg1Nz Ring.IntID.mulrN (@powg1Nz g (m * (-n))) expMnz.
qed.

lemma correctness g (k c sk : zmod):  g ^ (k + c * sk) * g ^ sk ^ -c =  g ^ k.
proof.
rewrite {2}/(^) expMz intpow_mod -mulE expDz intpow_mod -addE.
rewrite ZModpRing.mulrN ComRing.mulrC -ZModule.addrA ZModpRing.subrr.
by rewrite ZModpRing.addr0.
qed.

lemma logM (g g' : _GP) (x : zmod): g <> eg => log g (g' ^ x) = x * log g g'.
proof.
move=> g_neq1; rewrite -{1}(@powK g g') // /(^)  expMz intpow_mod -mulE logK //.
by rewrite ZModpRing.mulrC.
qed.

lemma logD (g x y : _GP): g <> eg => log g (x * y) = log g x + log g y.
proof.
move=> g_neq1; rewrite -{1}(@powK g x) 2:-{1}(@powK g y)  // expDz.
by rewrite intpow_mod -addE logK.
qed.

end Group_GP.

theory SigScheme2ROs.
type pkey, skey, msg, sig.
type hto, hfrom, h'to, h'from.

module type PubRO1 = {
  proc h(_: hfrom): hto
}.

module type RO1 = {
  proc init()     : unit
  include PubRO1
}.

module type PubRO2 = {
  proc g(_: h'from): h'to
}.

module type RO2 = {
  proc init()       : unit
  include PubRO2
}.

(* Def of Signature Scheme *)
module type Sig_Scheme (H : PubRO1) (H' : PubRO2) = {
  proc gen(): pkey * skey
  proc sign(_: skey * msg): sig
  proc ver(_: pkey * msg * sig ): bool
}.

module type SigOracle = {
  proc sign(_: msg): sig
}.

module type Adv_CMA (O: SigOracle) (H : PubRO1) (H' : PubRO2) = {
  proc forge (_: pkey): (msg * sig)
}.

module EUF_CMA (S : Sig_Scheme) (H : RO1) (G : RO2) (A : Adv_CMA) = {
  var sk : skey
  var qs : msg list

  module Sig : SigOracle = {
    proc sign(m : msg) = {
      var r;

      qs <- m :: qs;
      r  <@ S(H,G).sign(sk, m);
      return r;
    }
  }

  proc main (): bool = {
    var pk, m, sig, forged;

          qs <- [];
                H.init();
                G.init();
     (pk,sk) <@ S(H,G).gen();
    (m, sig) <@ A(Sig,H,G).forge(pk);
      forged <@ S(H,G).ver(pk, m, sig);

    return forged /\ !m \in qs;
  }
}.
end SigScheme2ROs.

(*- Def of CDH assumption *)
(* The security of EDL sig scheme relies on the hardness of 
   the CDH problem  *)
import Group_GP.

module type Adv_CDH = {
  proc solve(ga gb: _GP) : _GP
}.

module CDH (A: Adv_CDH) = {
  proc main (): bool = {
    var a, b, r;

    a <$ dp;
    b <$ dp;
    r <@ A.solve(gg ^ a, gg ^ b);
    return r = gg ^ (a * b);
  }
}.

(** Defining EDL **)
type pkey = _GP.
type skey = zmod.

type msg.

(* Nonce *)
type rand.

op drand: rand distr.
axiom drand_ll : is_lossless drand.

op drand_max: { real | forall x, mu1 drand x <= drand_max}
  as drand_max_ub.

lemma gt0_drand_max: 0%r < drand_max.
proof. smt(witness_support drand_ll drand_max_ub). qed.

clone import SigScheme2ROs as SIG with
  type pkey <- pkey, 
  type skey <- skey,
  type msg  <- msg,
  type sig  <- (_GP * rand  * zmod * zmod),
  type hto  <- _GP,
  type hfrom<- msg * rand,
  type h'to <- zmod,
  type h'from<- _GP * _GP * _GP * _GP * _GP * _GP.

(***** hash H and G *)
clone import ROM as Ht with
  type d_in_t  <- unit,
  type d_out_t <- unit,
  type in_t    <- msg * rand,
  type out_t   <- _GP,
  op   dout _  <- dgp.

module H = {
  proc init = Ht.Lazy.LRO.init
  proc h    = Ht.Lazy.LRO.o
}.

clone import ROM as Gt with
  type d_in_t  <- unit,
  type d_out_t <- unit,
  type in_t    <- _GP * _GP * _GP * _GP * _GP * _GP,
  type out_t   <- zmod,
  op   dout _  <- dp.

module G = {
  proc init = Gt.Lazy.LRO.init
  proc g    = Gt.Lazy.LRO.o
}.

(***** r-producing PRF — seen as a private RF for now ***)
clone import ROM as Rt with
  type d_in_t  <- unit,
  type d_out_t <- unit,
  type in_t    <- msg, (** for tightness in a multi-user setting, we'll want pkey as input to H **)
  type out_t   <- rand,
  op   dout _  <- drand.

module F = {
  proc init = Rt.Lazy.LRO.init
  proc f    = Rt.Lazy.LRO.o
}.

(** Definition **)
module (EDL_Sig : Sig_Scheme) (H : PubRO1) (G : PubRO2) = {
  proc gen(): pkey * skey   = {
    var pk, sk;

    sk <$ dp;
    pk <- gg ^ sk;
          F.init();
    return (pk, sk);
  }

  proc sign(sk: skey, msg: msg): (_GP * rand * zmod * zmod) = {
    var  r, z, c, h, k, u, v,  s;

    k <$ dp;
    u <- gg ^ k;

    r <@ F.f(msg);
    h <@ H.h(msg, r);

    z <- h ^ sk;
    v <- h ^ k;

    c <@ G.g(gg, h, gg ^ sk, z, u, v);
    s <- k + c * sk;

    return (z, r, s, c);
  }

  proc ver(pk: pkey, msg: msg, sig: (_GP * rand * zmod * zmod)) : bool = {
    var h, r, u, s, z, c, v, c';

    (z,r,s,c) <- sig;

            u <- gg ^ s * pk ^ -c;
            h <@ H.h(msg, r);

            v <- h ^ s * z ^ -c;
           c' <@ G.g(gg, h, pk, z, u,v);

    return c = c';
  }
}.

(****************** Security Proof ************************)
type h_resp =
[ AHQuery of zmod
| SHQuery of zmod ].

op d'_get x =
with x = AHQuery d => d
with x = SHQuery d => d.

op h_get gb x =
with x = AHQuery d => gb * gg ^ d
with x = SHQuery d => gg ^ d.

module Simulator (A : Adv_CMA) = {
  var pk : _GP
  var gb : _GP

  module H = {
    var m : (msg * rand, h_resp) fmap

    proc h(x) = {
      var d;

      d <$ dp;
      if (x \notin m) {
        m.[x] <- AHQuery d;
      }
      return oapp (h_get gb) witness m.[x];
    }
  }

  module G = {
    var m : (_GP * _GP * _GP * _GP * _GP * _GP, zmod) fmap

    proc g(x) = { 
     var c;

      c <$ dp;
      if (x \notin m) {
        m.[x] <- c;
      }
      return oget m.[x];
    }
  }

  module S : SigOracle = {
    proc sign(m : msg) = {
      var r, z, c, d, h, u, v, s;

                               r <@ F.f(m);
                               d <$ dp;
                               c <$ dp;
                               s <$ dp;

                               d <- if   (exists d, H.m.[m, r] = Some (SHQuery d))
                                    then oapp d'_get witness H.m.[m, r]
                                    else d;

                               h <- gg ^ d;
                               z <- pk ^ d;
                               u <- gg ^ s * pk ^ -c;
                               v <-  h ^ s *  z ^ -c;

                     H.m.[(m,r)] <- SHQuery d;
      G.m.[(gg, h, pk, z, u, v)] <- c;
      return (z, r, s, c);
    }
  }

  proc solve(ga : _GP, gb : _GP) : _GP = {
    var m, sig, z, r, s, c, d;

             H.m <- empty;
             G.m <- empty;
              pk <- ga;
    Simulator.gb <- gb;
                    F.init();

        (m, sig) <@ A(S,H,G).forge(pk);
    (z, r, s, c) <- sig;

    d            <$ dp;
    d            <- oapp d'_get d H.m.[m ,r];
    return z * ga ^ -d;
  }
}.

module (CountingA (A : Adv_CMA) : Adv_CMA) (O : SigOracle) (H : PubRO1) (G : PubRO2) = {
  var cH : int
  var cG : int
  var cS : int

  module Ho = {
    proc h(x) = {
      var r <- witness;

      r  <@ H.h(x);
      cH <- cH + 1;
      return r;
    }
  }

  module Go = {
    proc g(x) = {
      var r <- witness;

      r   <@ G.g(x);
      cG <- cG + 1;
      return r;
    }
  }

  module S = {
    proc sign(m) = {
      var r <- witness;

      r  <@ O.sign(m);
      cS <- cS + 1;
      return r;
    }
  }

  proc forge(pk) = {
    var r;

    cH <- 0;
    cG <- 0;
    cS <- 0;
    r  <@ A(S,Ho,Go).forge(pk);
    return r;
  }
}.

op qH : { int | 0 <= qH } as ge0_qH.
op qG : { int | 0 <= qG } as ge0_qG.
op qS : { int | 0 <= qS } as ge0_qS.

(*** Game Sequences ***)
module type GOracles = {
  proc init(): unit

  include PubRO1
  proc hV(x : msg * rand): _GP

  include PubRO2
  proc gV(x : _GP * _GP * _GP * _GP * _GP * _GP): zmod

  include SigOracle
}.

section Proofs.
declare module A <: Adv_CMA {-Ht.Lazy.LRO, -Gt.Lazy.LRO, -EUF_CMA, -Simulator, -CountingA}.

declare axiom forge_ll (O <: SigOracle { -A }) (H <: PubRO1 { -A }) (G <: PubRO2 { -A }):
     islossless G.g
  => islossless H.h
  => islossless O.sign
  => islossless A(O, H, G).forge.

declare axiom forge_qs (S <: SigOracle { -A, -CountingA })
               (H <: PubRO1    { -A, -CountingA })
               (G <: PubRO2    { -A, -CountingA }):
  hoare [A(CountingA(A,S,H,G).S,CountingA(A,S,H,G).Ho,CountingA(A,S,H,G).Go).forge:
              CountingA.cS = 0
           /\ CountingA.cH = 0
           /\ CountingA.cG = 0
           ==>    CountingA.cS <= qS
               /\ CountingA.cH <= qH
               /\ CountingA.cG <= qG].

local module Game (O : GOracles) = {
  var sk : zmod
  var pk : _GP
  var qs : msg list
  var  h : _GP
  var  z : _GP

  proc init() = {
          F.init();
          O.init();
    qs <- [];
    sk <$ dp;
    pk <- gg ^ sk;
  }

  module S = {
    proc sign(m : msg) = {
      var r;

      qs <- m :: qs;
       r <@ O.sign(m);
      return r;
    }
  }

  proc main (): bool = {
    var m, sig, r, s, c, u, v, c';

                       init();
           (m, sig) <@ A(S, O, O).forge(pk);
       (z, r, s, c) <- sig;

                  u <- gg ^ s * pk ^ -c;
                  h <@ O.hV(m, r);
                  v <- h ^ s * z ^ -c;
                 c' <@ O.gV(gg, h, pk, z, u,v);

    return c = c' /\ !m \in qs;
  }
}.

(** We can't use the following directly because of the (overly
    conservative) restriction over Game. The argument simply replays
    in all settings, however, since the oracles use Game's memory as
    if through a get/set interface.                                   **)
local lemma Game_equiv (O1 <: GOracles { -Game, -A }) (O2 <: GOracles { -Game, -A }) (Inv : glob O1 -> glob O2 -> bool) :
     equiv [ O1.init ~ O2.init : true ==> Inv (glob O1){1} (glob O2){2} ]
  => equiv [ O1.sign ~ O2.sign : ={arg} /\ Inv (glob O1){1} (glob O2){2} ==> ={res} /\ Inv (glob O1){1} (glob O2){2} ]
  => equiv [ O1.h    ~ O2.h    : ={arg} /\ Inv (glob O1){1} (glob O2){2} ==> ={res} /\ Inv (glob O1){1} (glob O2){2} ]
  => equiv [ O1.hV   ~ O2.hV   : ={arg} /\ Inv (glob O1){1} (glob O2){2} ==> ={res} /\ Inv (glob O1){1} (glob O2){2} ]
  => equiv [ O1.g    ~ O2.g    : ={arg} /\ Inv (glob O1){1} (glob O2){2} ==> ={res} /\ Inv (glob O1){1} (glob O2){2} ]
  => equiv [ O1.gV   ~ O2.gV   : ={arg} /\ Inv (glob O1){1} (glob O2){2} ==> ={res} /\ Inv (glob O1){1} (glob O2){2} ]
  => equiv [ Game(O1).main ~ Game(O2).main: ={glob A} ==> ={glob A} /\ Inv (glob O1){1} (glob O2){2} ].
proof.
move=> eq_init eq_sign eq_h eq_hV eq_g eq_gV; proc.
call eq_gV.
wp.
call eq_hV.
wp.
call (: Inv (glob O1){1} (glob O2){2}).
+ by proc; call eq_sign; auto.
by inline *; auto; call eq_init; auto.
qed.

(* Game 1: embed gb in adversary queries to H, and
           sample H responses to signing so that we know their discrete log *)
type h'_resp =
[ AH'Query of zmod
| SH'Query of zmod & zmod & zmod ].

op h'_get gb x =
with x = AH'Query d     => gb * gg ^ d
with x = SH'Query d s c => gg ^ d.

op d_get x =
with x = AH'Query d     => d
with x = SH'Query d s c => d.

op to_sim x =
with x = AH'Query d     => AHQuery d
with x = SH'Query d s c => SHQuery d.

local module H' = {
  var gb  : _GP
  var m   : (msg * rand, h'_resp)  fmap

  proc init(): unit = {
    var b;

     m <- empty;
     b <$ dp;
    gb <- gg ^ b;
  }

  proc h(x) : _GP = {
    var d;

    d <$ dp;
    if (x \notin m) {
      m.[x] <- AH'Query d;
    }
      
    return oapp (h'_get gb) witness m.[x];
  }

  proc hV(x) = {
    var d, h;

    d <$ dp;
    if (x \in H'.m) {
      h <- oapp (h'_get H'.gb) witness H'.m.[x];
    } else {
      h <- H'.gb * gg ^ d;
    }
    return h;
  }
}.

local equiv hA_eq:
  H.h ~ H'.h:
        ={arg}
    /\ Ht.Lazy.LRO.m{1} = (map (fun _=> h'_get H'.gb) H'.m){2}
    /\ (forall m r d s c, H'.m.[m, r] = Some (SH'Query d s c) => Rt.Lazy.LRO.m.[m] = Some r){2}
    ==>    ={res}
        /\ Ht.Lazy.LRO.m{1} = (map (fun _=> h'_get H'.gb) H'.m){2}
        /\ (forall m r d s c, H'.m.[m, r] = Some (SH'Query d s c) => Rt.Lazy.LRO.m.[m] = Some r){2}.
proof.
proc; wp; rnd (fun h=> log gg (h * inv H'.gb){2}) (fun d=> H'.gb{2} * gg ^ d).
skip=> /> &2 inv; do 2?(split=> [|_]).
+ move=> d _; rewrite !logD ?logK ?g_n //.
  rewrite -ZModule.addrA ZModule.addrC -ZModule.addrA (@ZModule.addrC (log gg (inv H'.gb{2}))).
  by rewrite -logD ?g_n // subrr -(@powg10z gg) -zeroE logK ?g_n // ZModpRing.addr0.
+ by move=> d _; rewrite dp1E dgp1E.
move=> y _; do 2?(split=> [|_]).
+ by rewrite dp_fu.
+ by rewrite powK ?g_n // mulg1CA subrr mulg10.
rewrite mem_map !get_set_sameE !map_set=> />; split=> />.
+ rewrite powK ?g_n // mulg1CA subrr mulg10=> //= _ m r d s c.
  by rewrite get_setE; case: ((m, r) = x{2})=> /> _ /inv.
by rewrite mapE; case: (H'.m.[x]{2}).
qed.

local equiv hV_eq:
  H.h ~ H'.hV:
       ={arg}
    /\ Ht.Lazy.LRO.m{1} = (map (fun _=> h'_get H'.gb) H'.m){2}
    ==> ={res}.
proof.
proc. case: ((x \in H'.m){2}).
+ rcondf{1} 2; first by auto=> /> + _ _; rewrite !domE mapE /#.
  rcondt{2} 2; first by auto.
  auto; rnd{1}; rnd{2}; auto; rewrite dp_ll dgp_ll=> /> &2 _ _ _ _ _.
  by rewrite mapE; case: (H'.m.[x]{2}).
rcondt{1} 2; first by auto=> /> + _ _; rewrite !domE mapE /#.
rcondf{2} 2; first by auto.
wp; rnd (fun h=> log gg (h * inv H'.gb){2}) (fun d=> H'.gb{2} * gg ^ d).
auto=> /> &2 _; split=> [d _|_].
+ rewrite !logD ?logK ?g_n //.
  rewrite -ZModule.addrA ZModule.addrC -ZModule.addrA (@ZModule.addrC (log gg (inv H'.gb{2}))).
  by rewrite -logD ?g_n // subrr -(@powg10z gg) -zeroE logK ?g_n // ZModpRing.addr0.
split=> [d _|_ h _].
+ by rewrite dp1E dgp1E.
rewrite dp_fu get_set_sameE //=.
by rewrite powK ?g_n // mulg1CA subrr mulg10.
qed.

local equiv hA_same: H'.h ~ H'.h: ={glob H', arg} ==> ={glob H', res}.
proof. by sim. qed.

local equiv hV_same: H'.hV ~ H'.hV: ={glob H', arg} ==> ={glob H', res}.
proof. by sim. qed.

local equiv g_same: G.g ~ G.g: ={glob Gt.Lazy.LRO, arg} ==> ={glob Gt.Lazy.LRO, res}.
proof. by sim. qed.

local module O1 = {
  var event1 : bool
  var event2 : bool

  proc init() = {
              H'.init();
              Gt.Lazy.LRO.init();
    event1 <- false;
    event2 <- false;
  }

  proc h  = H'.h
  proc hV = H'.hV

  proc g  = G.g

  proc gV(x) = {
    var c;

    c <$ dp;
    if (x \in Gt.Lazy.LRO.m) {
      c <- oget Gt.Lazy.LRO.m.[x];
    }
    return c;
  }    

  proc sign(m) = {
    var r, d, h, z, k, u, v, c, s;

               r <@ F.f(m);
               k <$ dp;
               u <- gg ^ k;

               d <$ dp;
               d <- if   exists d s c, H'.m.[m, r] = Some (SH'Query d s c)
                    then oapp d_get witness H'.m.[m, r]
                    else d;
               h <- gg ^ d;
               z <- h ^ Game.sk;

               v <-  h ^ k;
          event2 <- event2 \/ (gg, h, gg ^ Game.sk, z, u, v) \in Gt.Lazy.LRO.m;
               c <@ G.g(gg, h, Game.pk, z, u, v);
               s <- k + c * Game.sk;
          event1 <- event1 \/ exists d, H'.m.[m, r] = Some (AH'Query d);
    H'.m.[(m,r)] <- SH'Query d s c;
    return (z, r, s, c);
  }
}.

local module Game1 = Game(O1).

local lemma pr_CMA_Game1 &m:
  Pr[EUF_CMA(EDL_Sig, H, G, A).main() @ &m: res]
  <= Pr[Game1.main() @ &m: res] + Pr[Game1.main() @ &m: O1.event1].
proof.
byequiv (: ={glob A} ==> !O1.event1{2} => ={res})=> // [|/#].
proc. inline EDL_Sig(H, G).ver.
wp; conseq (: !O1.event1{2} => ={c, c', m} /\ ={qs}(EUF_CMA,Game))=> [/#|].
seq  4  1: (   ={glob A, glob F, Gt.Lazy.LRO.m}
            /\ ={sk, qs}(EUF_CMA,Game)
            /\ pk{1} = Game.pk{2}
            /\ Ht.Lazy.LRO.m{1} = (map (fun x=> h'_get H'.gb) H'.m){2}
            /\ Game.pk{2} = gg ^ Game.sk{2}
            /\ (forall m r d s c, H'.m.[m, r] = Some (SH'Query d s c) => Rt.Lazy.LRO.m.[m] = Some r){2}).
+ inline *; auto; rewrite dp_ll=> /> b _ _ _.
  by rewrite map_empty=> //= m r d s c; rewrite emptyE.
seq  1  1: (!O1.event1{2}
            => (   ={glob A, glob F, Gt.Lazy.LRO.m, m, sig}
                /\ ={sk, qs}(EUF_CMA,Game)
                /\ pk{1} = Game.pk{2}
                /\ Ht.Lazy.LRO.m{1} = (map (fun x=> h'_get H'.gb) H'.m){2})
                /\ (forall m r d s c, H'.m.[m, r] = Some (SH'Query d s c) => Rt.Lazy.LRO.m.[m] = Some r){2}).
+ call (: O1.event1,
             ={glob F, Gt.Lazy.LRO.m}
          /\ ={sk, qs}(EUF_CMA,Game)
          /\ Ht.Lazy.LRO.m{1} = (map (fun x=> h'_get H'.gb) H'.m){2}
          /\ Game.pk{2} = gg ^ Game.sk{2}
          /\ (forall m r d s c, H'.m.[m, r] = Some (SH'Query d s c) => Rt.Lazy.LRO.m.[m] = Some r){2}).
  + exact/forge_ll.
  + by conseq g_same.
  + by move=> _ _; islossless; exact/dp_ll.
  + by move=> _; conseq (: true); islossless; exact/dp_ll.
  + by conseq hA_eq.
  + by move=> _ _; islossless; exact/dgp_ll.
  + by move=> _; conseq (: true); islossless; exact/dp_ll.
  + proc; inline F.f EDL_Sig(H, G).sign O1.sign.
    seq  9  8: (   ={glob F, Gt.Lazy.LRO.m, m, k, u, r0}
                /\ ={sk, qs}(EUF_CMA, Game)
                /\ msg{1} = m0{2}
                /\ Ht.Lazy.LRO.m{1} = (map (fun _=> h'_get H'.gb) H'.m){2}
                /\ !O1.event1{2}
                /\ sk{1} = EUF_CMA.sk{1}
                /\ m0{2} = m{2}
                /\ Game.pk{2} = gg ^ Game.sk{2}
                /\ (forall m r d s c, H'.m.[m, r] = Some (SH'Query d s c) => Rt.Lazy.LRO.m.[m] = Some r){2}
                /\ (Rt.Lazy.LRO.m.[m0] = Some r0){2}).
    + swap {2} 7 -3; auto=> /> &2 _ inv _ _ r _; split=> [|/#] r_notin_F.
      split=> [m0 r0 d0 s0 c0|].
      + rewrite get_setE; case: (m0 = m{2})=> /> => [|_ /inv //].
        by move: r_notin_F=> + /inv; rewrite domE=> /= ->.
      by rewrite get_set_sameE.
    case: (exists d, H'.m.[m, r0]{2} = Some (AH'Query d)).
    + conseq (: O1.event1{2})=> //=.
      by inline *; auto; rnd{1}; rnd{2}; auto=> />; rewrite dp_ll dgp_ll.
    inline H.h.
    wp; call g_same; inline H.h; auto.
    rnd (log gg) (fun x=> gg ^ x); auto=> /> &2 ne1 inv F_m /negb_exists /= u_notin_H.
    split=> [d _|_]; first by rewrite logK 1:g_n.
    split=> [d _|_ h _]; first by rewrite dp1E dgp1E.
    rewrite dp_fu /= powK 1:g_n //=.
    rewrite mem_map get_set_sameE //= !mapE //=.
    rewrite domE /=; move: u_notin_H.
    case: {-1}(H'.m.[m, r0]{2}) (eq_refl H'.m.[m, r0]{2})=> /> => [H'm|[d0 _ /(_ d0) //|d0 s0 c0 H'm _ /=]].
    + rewrite powK 1:g_n=> //= c.
      rewrite map_set /= powK 1:g_n=> //= m' r' d' c' s'.
      by rewrite get_setE; case: ((m', r') = (m, r0){2})=> /> _ /inv.
    have -> /=: exists d s c, d0 = d /\ s0 = s /\ c0 = c; first by exists d0 s0 c0.
    move=> c; rewrite map_set /=; split.
    + apply: fmap_eqP=> - [m' r']; rewrite get_setE.
      by case: ((m', r') = (m, r0){2})=> />; rewrite mapE H'm.
    move=> m' r' d' s' c'; rewrite get_setE; case: ((m', r') = (m, r0){2})=> />.
    by move=> _ /inv.
  + by move=> _ _; islossless; smt(dp_ll dgp_ll drand_ll).
  + by move=> _; proc; inline *; auto; smt(dp_ll drand_ll).
  by auto=> /#.
case: (O1.event1{2}).
+ conseq (: O1.event1{2})=> //; inline *; auto; rnd{1}; rnd{2}; auto=> />.
  smt(dp_ll dgp_ll).
call (: ={glob Gt.Lazy.LRO, arg} ==> ={res}).
+ by proc; auto=> /> &2 r _; rewrite get_set_sameE.
by auto; call hV_eq; auto=> /#.
qed.

(* Game 2: simulate the proof without sk *)
type g'_resp =
[ AG'Query of zmod
| SG'Query of zmod ].

op c_get x =
with x = AG'Query c => c
with x = SG'Query c => c.

local module G' = {
  var unsound : bool
  var scol    : bool
  var gcol    : bool
  var m       : (_GP * _GP * _GP * _GP * _GP * _GP, g'_resp)  fmap

  proc init(): unit = {
    unsound <- false;
       scol <- false;
       gcol <- false;
          m <- empty;
  }

  proc g(x) = {
    var c;

       c <$ dp;
    if (x \notin m) {
         gcol <- gcol \/ exists y, m.[y] = Some (SG'Query c);
      unsound <-    unsound
                 \/ (   x.`1 = gg
                     /\ x.`4 <> x.`2 ^ (log gg x.`3)
                     /\   (x.`5 * x.`3 ^ c) ^ (log gg x.`2)
                        = x.`6 * x.`4 ^ c);
        m.[x] <- AG'Query c;
    }

    return oapp c_get witness m.[x];
  }
}.

local equiv gA_eq:
  G.g ~ G'.g:
    ={arg} /\ Gt.Lazy.LRO.m{1} = map (fun _=> c_get) G'.m{2}
    ==> ={res} /\ Gt.Lazy.LRO.m{1} = map (fun _=> c_get) G'.m{2}.
proof.
proc; auto=> /> &2 c _; rewrite !get_set_sameE mem_map !domE !map_set mapE=> />.
by case: (G'.m.[x]{2}).
qed.

local equiv gA_same:
  G'.g ~ G'.g: ={glob G', arg} ==> ={glob G', res}.
proof. by sim. qed.

local module O2 = {
  var event2 : bool

  proc init() = {
              H'.init();
              G'.init();
    event2 <- false;
  }

  proc h  = H'.h
  proc hV = H'.hV

  proc g  = G'.g
  proc gV = G'.g

  proc sign(m : msg) = {
    var r, d, h, z, k, u, v, c, s;

                                   r <@ F.f(m);
                                   d <$ dp;
                                   c <$ dp;
                                   s <$ dp;
                                   k <- (s - c * Game.sk)%PFo;

                                   d <- if   exists d s c, H'.m.[m, r] = Some (SH'Query d s c)
                                        then oapp d_get witness H'.m.[m, r]
                                        else d;

                                   u <- gg ^ k;
                                   h <- gg ^ d;
                                   z <- h ^ Game.sk;
                                   v <- h ^ k;

                             G'.scol <- G'.scol \/ exists x, G'.m.[x] = Some (AG'Query c);
                              event2 <- event2 \/ (gg, h, gg ^ Game.sk, z, u, v) \in G'.m;
                       H'.m.[(m, r)] <- SH'Query d s c;
    G'.m.[(gg, h, Game.pk, z, u, v)] <- SG'Query c;
    return (z, r, s, c);
  }
}.

local module Game2 = Game(O2).

local equiv eq_sign1_sign2: O1.sign ~ O2.sign:
     !O2.event2{2}
  /\ ={event2}(O1, O2)
  /\ ={glob H', glob F, Game.pk, Game.sk, arg}
  /\ Gt.Lazy.LRO.m{1} = map (fun _=> c_get) G'.m{2}
  /\ Game.pk{2} = gg ^ Game.sk{2}
  ==>    ={event2}(O1, O2)
      /\ Game.pk{2} = gg ^ Game.sk{2}
      /\ (!O2.event2{2}
          => (   ={glob H', glob F, Game.pk, Game.sk, res}
              /\ Gt.Lazy.LRO.m{1} = map (fun _=> c_get) G'.m{2})).
proof.
proc.
inline G.g.
swap{1} [4..7] -2. swap{1} 11 -5. swap{2} [8..9] -1. swap{2} [6..8] -3.
seq  6  6: (#pre /\ ={r, d, h, z} /\ r0{1} = c{2}); first by inline *; auto.
seq  1  2: (#pre /\ ={k} /\ (s = k + c * Game.sk){2}).
+ wp; rnd (fun k=> k + c * Game.sk){2} (fun s=> s - c * Game.sk)%PFo{2}.
  skip=> &1 &2 /> _; split=> [s _|_]; first by algebra.
  split=> [s _|_ k _]; first by rewrite !dp1E.
  by rewrite dp_fu=> /=; split=> [|_]; algebra.
seq  2  2: (#pre /\ ={u, v}); first by auto.
case: (((gg, h, gg ^ Game.sk, z, u, v) \in Gt.Lazy.LRO.m){1}).
+ conseq (: true)
         (: (gg, h, gg ^ Game.sk, z, u, v) \in Gt.Lazy.LRO.m ==> O1.event2)
         (: (gg, h, gg ^ Game.sk, z, u, v) \in G'.m ==> O2.event2)=> />.
  + by move=> &0; rewrite mem_map.
  + by auto=> />.
  + by auto=> />.
  by auto.
rcondt{1} 3; first by auto.
wp; skip=> &1 &2 [#] /= ne2 + + + + + + + ->.
by rewrite mem_map get_set_sameE map_set.
qed.

local equiv eq_Game1_Game2:
  Game1.main ~ Game2.main:
    ={glob A, glob F}
    ==> ={event2}(O1, O2) /\ (!O2.event2{2} => ={res}).
proof.
proc.
seq  2  2: (   ={event2}(O1, O2)
            /\ Game.pk{2} = gg ^ Game.sk{2}
            /\ (   !O2.event2{2}
                =>    ={glob H', glob A, glob F, Game.sk, Game.pk, Game.qs, m, sig}
                   /\ ={event2}(O1, O2)
                   /\ Gt.Lazy.LRO.m{1} = map (fun _=> c_get) G'.m{2})).
+ call (: O2.event2,
             ={glob H', glob F, Game.sk, Game.pk, Game.qs}
          /\ ={event2}(O1, O2)
          /\ Gt.Lazy.LRO.m{1} = map (fun _=> c_get) G'.m{2}
          /\ Game.pk{2} = gg ^ Game.sk{2}
         ,   ={event2}(O1, O2)
          /\ Game.pk{2} = gg ^ Game.sk{2}).
  + exact/forge_ll.
  + by conseq gA_eq.
  + by move=> &2 _; conseq (: true); islossless; exact/dp_ll.
  + by move=> &1; conseq (: true); islossless; exact/dp_ll.
  + by conseq hA_same.
  + by move=> &2 _; conseq (: true); islossless; exact/dp_ll.
  + by move=> &1; conseq (: true); islossless; exact/dp_ll.
  + by proc; call eq_sign1_sign2; auto=> /#.
  + by move=> &2 ->; proc; inline *; auto=> />; rewrite dp_ll drand_ll.
  + by move=> &1; proc; inline *; auto=> />; rewrite dp_ll drand_ll.
  by inline *; auto=> />; rewrite map_empty /#.
case: (O2.event2{2}).
+ by conseq (: ={event2}(O1, O2))=> />; inline *; auto.
call (:    Gt.Lazy.LRO.m{1} = map (fun _=> c_get) G'.m{2}
        /\ ={arg}
        ==> ={res}).
+ proc; auto=> /> &2 c _; rewrite mem_map get_set_sameE mapE !domE.
  by case: (G'.m.[x]{2}).
by auto; call hV_same; auto=> /#.
qed.

local lemma pr_Game1_Game2 &m:
     Pr[Game1.main() @ &m: res]
  <= Pr[Game2.main() @ &m: res] + Pr[Game1.main() @ &m: O1.event2].
proof.
have ->:   Pr[Game1.main() @ &m: O1.event2]
         = Pr[Game2.main() @ &m: O2.event2].
+ by byequiv eq_Game1_Game2.
by byequiv eq_Game1_Game2=> /#.
qed.

(** Reduction **)
inductive mr_split (m : msg) (r : rand)
                   (h' : (_, _) fmap) =
| NotInList       of (h'.[(m, r)] = None)
| InUList   d     of (h'.[(m, r)] = Some (AH'Query d))
| InYList   d s c of (h'.[(m,r)] = Some (SH'Query d s c)).

(** TODO: invariance lemmas **)
op fresh (qs : msg list) (h' : (_, h'_resp) fmap) (m : msg) =
  !m \in qs
  => forall (r : rand) d s c, h'.[(m, r)] <> Some (SH'Query d s c).

(** TODO: invariance lemmas for other Hoare conditions **)

(*** FIXME: simulator now must keep track of whether an H-query was made by the forger or its signing oracle, since we bind randomness to message **)

local lemma Reduction &m:
  Pr[Game2.main() @ &m: res]
  <=   Pr[CDH(Simulator(A)).main() @ &m: res]
     + Pr[Game2.main() @ &m:    G'.unsound].
proof.
rewrite Pr [mu_split (Game.z = Game.h ^ Game.sk)].
apply/ler_add.
+ byequiv (: ={glob A}
             ==> (   res
                  /\ Game.z = Game.h ^ Game.sk){1}
                 => res{2})=> [|//|/#].
  proc; inline Simulator(A).solve.
  seq  3 11: (   ={glob A, glob F, m, sig, s, c}
              /\ r{1}       = r0{2}
              /\ H'.gb{1}   = Simulator.gb{2}
              /\ Game.pk{1} = Simulator.pk{2}
              /\ Game.pk{1} = gg ^ a{2}
              /\ Game.sk{1} = a{2}
              /\ Game.z{1}  = z{2}
              /\ H'.gb{1}   = gg ^ b{2}
              /\ ga{2}      = gg ^ a{2}
              /\ Game.pk{1} = gg ^ Game.sk{1}
              /\ (map (fun _=> to_sim) H'.m){1} = Simulator.H.m{2}
              /\ map (fun _=> c_get) G'.m{1}          = Simulator.G.m{2}
              /\ (forall m r, mr_split m r H'.m){1}
              /\ (forall m, fresh Game.qs H'.m m){1}).
  + wp; call (:    ={glob F}
                /\ H'.gb{1}   = Simulator.gb{2}
                /\ Game.pk{1} = Simulator.pk{2}
                /\ Game.pk{1} = gg ^ Game.sk{1}
                /\ (map (fun _=> to_sim) H'.m){1} = Simulator.H.m{2}
                /\ map (fun _=> c_get) G'.m{1}          = Simulator.G.m{2}
                /\ (forall m r, mr_split m r H'.m){1}
                /\ (forall m, fresh Game.qs H'.m m){1}).
    + proc; auto=> /> &1 &2 ih fr c' _; rewrite !mem_map !get_set_sameE map_set=> />.
      by rewrite !domE /= mapE; case: (G'.m{1}.[x{2}]).
    + proc; auto=> /> &1 &2 ih fr d' _.
      rewrite mem_map !domE !get_set_sameE map_set !mapE=> />.
      case: {-1}(H'.m{1}.[x{2}]) (eq_refl (H'.m{1}.[x{2}]))=> //= [H'x |[] //].
      split=> [m r|m].
      + case: ((m, r) = x{2})=> /> => [|mr_neq_x].
        + exact/(InUList d')/get_set_sameE.
        case: (ih m r)=> [H'mr|d H'u|d s c /> H'mr].
        + by apply/NotInList; rewrite get_set_neqE.
        + by apply/(InUList d); rewrite get_set_neqE.
        by apply/(InYList d s c); rewrite get_set_neqE.
      move=> /fr + r d s c - /(_ r); rewrite get_setE.
      by case: ((m,r) = x{2})=> /> _ /(_ d s c).
    + proc. inline O2.sign. swap{1} [2..3] -1.
      seq  2  1: (#pre /\ m0{1} = m{1} /\ r0{1} = r{2}).
      + by inline F.f; auto.
      auto=> /> &1 &2 ih fr d' _ c' _ s' _.
      rewrite !mapE !map_set /=.
      case: {-1}(H'.m{1}.[m, r]{2}) (eq_refl H'.m{1}.[m, r]{2})=> /> => [|[d0|d0 s0 c0]] H'mr' />.
      + split.
        + by rewrite /(^) expMz mulzC -expMz.
        split.
        + congr=> //=; split.
          + by rewrite /(^) expMz mulzC -expMz.
          split.
          + by rewrite {3}/Group_GP.(^) expMz intpow_mod -mulE expDz intpow_mod -addE mulrN mulrC.
          rewrite /(^) expMz intpow_mod -mulE mulrDr addE -intpow_mod -expDz.
          rewrite mulE -intpow_mod -expMz; congr; rewrite eq_sym.
          by rewrite expMz intpow_mod -mulE expMz intpow_mod -mulE; algebra.
        split=> m' => [r'|@/fresh //=].
        + case: ((m', r') = (m, r){2})=> /> => [|u_neq_x].
          + by apply/(InYList d' s' c'); rewrite get_set_sameE.
          case: (ih m' r')=> [H'mr|d H'mr|d s c /> H'mr].
          + by apply/NotInList; rewrite get_set_neqE.
          + by apply/(InUList d); rewrite get_set_neqE.
          by apply/(InYList d s c); rewrite get_set_neqE //= H'mr.
        rewrite negb_or=> - [] neq_m /fr fr_m r d s c.
        by rewrite get_set_neqE //= 1:neq_m //; exact/fr_m.
      + split.
        + by rewrite /(^) expMz mulzC -expMz.
        split.
        + congr=> //=; split.
          + by rewrite /(^) expMz mulzC -expMz.
          split.
          + by rewrite {3}/Group_GP.(^) expMz intpow_mod -mulE expDz intpow_mod -addE mulrN mulrC.
          rewrite /(^) expMz intpow_mod -mulE mulrDr addE -intpow_mod -expDz.
          rewrite mulE -intpow_mod -expMz; congr; rewrite eq_sym.
          by rewrite expMz intpow_mod -mulE expMz intpow_mod -mulE; algebra.
        split=> m' => [r'|@/fresh //=].
        + case: ((m', r') = (m, r){2})=> /> => [|u_neq_x].
          + by apply/(InYList d' s' c'); rewrite get_set_sameE.
          case: (ih m' r')=> [H'mr|d H'mr|d s c /> H'mr].
          + by apply/NotInList; rewrite get_set_neqE.
          + by apply/(InUList d); rewrite get_set_neqE.
          by apply/(InYList d s c); rewrite get_set_neqE //= H'mr.
        rewrite negb_or=> - [] neq_m /fr fr_m r d s c.
        by rewrite get_set_neqE //= 1:neq_m //; exact/fr_m.
      have -> /=: exists d s c, d0 = d /\ s0 = s /\ c0 = c; first by exists d0 s0 c0.
      have -> /=: exists d, d0 = d; first by exists d0.
      split.
        + by rewrite /(^) expMz mulzC -expMz.
      split.
      + congr=> //=; split.
        + by rewrite /(^) expMz mulzC -expMz.
        split.
        + by rewrite {3}/Group_GP.(^) expMz intpow_mod -mulE expDz intpow_mod -addE mulrN mulrC.
        rewrite /(^) expMz intpow_mod -mulE mulrDr addE -intpow_mod -expDz.
        rewrite mulE -intpow_mod -expMz; congr; rewrite eq_sym.
        by rewrite expMz intpow_mod -mulE expMz intpow_mod -mulE; algebra.
      split=> m' => [r'|@/fresh //=].
      + case: ((m', r') = (m, r){2})=> /> => [|u_neq_x].
        + by apply/(InYList d0 s' c'); rewrite get_set_sameE.
        case: (ih m' r')=> [H'mr|d H'mr|d s c /> H'mr].
        + by apply/NotInList; rewrite get_set_neqE.
        + by apply/(InUList d); rewrite get_set_neqE.
        by apply/(InYList d s c); rewrite get_set_neqE //= H'mr.
      rewrite negb_or=> - [] neq_m /fr fr_m r d s c.
      by rewrite get_set_neqE //= 1:neq_m //; exact/fr_m.
    inline Game(O2).init O2.init H'.init G'.init F.init. swap{1} 11 -9.
    auto=> /> sk _ b _; rewrite !map_empty=> /=; split=> [m r|m _ r d s c].
    + exact/NotInList/emptyE.
    by rewrite emptyE.
  exists * m{1}, r{1}, H'.m{1}; elim * => m' r' h'.
  case @[ambient]: (mr_split m' r' h' = true); rewrite eqT; last first=> [not_mr_split|mr_split].
  + by exfalso=> &1 &2 [#] />; rewrite -negP=> /(_ m{2} r0{2}).
  case @[ambient]: mr_split=> [H'mr|d H'mr|d0 s0 c0 /> H'mr].
  + inline O2.hV. rcondf{1} 4; first by auto.
    inline O2.gV; wp; rnd{1}; auto=> &1 &2 />; rewrite dp_ll=> /= _ _ d' _ c' _.
    rewrite mapE H'mr.
    have -> //: ((gg ^ b * gg ^ d') ^ a * gg ^ a ^ -d'){2} = gg ^ (a * b){2}.
    rewrite {2}/Group_GP.(^) expMz intpow_mod -mulE.
    rewrite expDz intpow_mod -addE /(^) expMz intpow_mod -mulE.
    by rewrite expDz intpow_mod -addE /(^); algebra.
  + inline O2.hV. rcondt{1} 4; first by auto=> &2 />; rewrite domE H'mr.
    inline O2.gV; wp; rnd{1}; auto=> &1 &2 />; rewrite dp_ll=> /= _ _ d' _ c' _.
    rewrite !domE mapE get_set_sameE H'mr=> />.
    have -> //: ((gg ^ b * gg ^ d) ^ a * gg ^ a ^ -d){2} = gg ^ (a * b){2}.
    rewrite {2}/Group_GP.(^) expMz intpow_mod -mulE.
    rewrite expDz intpow_mod -addE /(^) expMz intpow_mod -mulE.
    by rewrite expDz intpow_mod -addE /(^); algebra.
  conseq (: _ ==> true)
         (:    H'.m = h'
            /\ m = m'
            /\ forall m, fresh Game.qs H'.m m ==> m \in Game.qs)=> />.
  + inline *; auto=> &1 /> + _ _ _ _ - /(_ m{1}) /contra h; apply/h.
    by rewrite negb_forall /=; exists r'; rewrite -negP=> /(_ d0 s0 c0) /=.
  by inline *; auto; rewrite dp_ll.
byequiv (: ={glob A, glob F} ==> _)=> //=; proc.
inline O2.gV.
wp 9 9.
conseq (: ={glob F, glob Game, glob H', glob G', c, m})
       (: _ ==>    x = (gg, Game.h, Game.pk, Game.z, u, v)
                /\ x \in G'.m
                /\ Game.pk = gg ^ Game.sk
                /\ u = gg ^ s * Game.pk ^ -c
                /\ v = Game.h ^ s * Game.z ^ -c
                /\ (forall m, fresh Game.qs H'.m m)
                /\ (forall (m : msg) (h y z u v : _GP) (c : zmod),
                         omap c_get G'.m.[(gg, h, y, z, u, v)] = Some c
                      => z <> h ^ (log gg y)
                      => (u * y ^ c) ^ log gg h = v * z ^ c
                      => G'.unsound))=> />.
+ move=> s G bad h qs sk z H c' m.
  pose u:= gg ^ s * gg ^ sk ^ -c'.
  pose v:= h ^ s * z ^ -c'.
  move=> x_in_G' frI unI c'P /frI fr neq_z.
  apply/(unI h (gg ^ sk) z u v c').
  + move: x_in_G' c'P; rewrite domE.
    by case: (G.[(gg, h, gg ^ sk, z, u, v)]).
  + by rewrite logK 1:g_n.
  move=> @/u @/v.
  rewrite /(^) expMz (@intpow_mod _ (asint _ * asint _)) -mulE.
  rewrite expDz (@intpow_mod _ (asint _ + asint _)) -addE mulg1C.
  rewrite expMz (@intpow_mod _ (asint _ * asint _)) -mulE.
  rewrite expDz (@intpow_mod _ (asint _ + asint _)) -addE.
  rewrite expMz intpow_mod -mulE eq_sym.
  rewrite -(@powK gg z) 1:g_n // !expMz !(@intpow_mod _ (asint _ * asint _)) -!mulE.
  rewrite -mulg1A expDz (@intpow_mod _ (asint _ + asint _)) -addE.
  rewrite -{1}(@powK gg h) 1:g_n //.
  rewrite mulg1C expMz (@intpow_mod _ (asint _ * asint _)) -mulE expDz intpow_mod -addE.
  by algebra.
+ seq  2: (   Game.pk = gg ^ Game.sk
           /\ (forall m, fresh Game.qs H'.m m)
           /\ (forall (m : msg) (h y z u v : _GP) (c : zmod),
                    omap c_get G'.m.[(gg, h, y, z, u, v)] = Some c
                 => z <> h ^ (log gg y)
                 => (u * y ^ c) ^ log gg h = v * z ^ c
                 => G'.unsound)).
  + call (:   Game.pk = gg ^ Game.sk
           /\ (forall m, fresh Game.qs H'.m m)
           /\ (forall (m : msg) (h y z u v : _GP) (c : zmod),
                    omap c_get G'.m.[(gg, h, y, z, u, v)] = Some c
                 => z <> h ^ (log gg y)
                 => (u * y ^ c) ^ log gg h = v * z ^ c
                 => G'.unsound)).
    + by proc; auto=> /> &hr frI unI c _ _ h y z u v c'; rewrite get_setE /#.
    + proc; auto=> /> &hr frI _ d' _ x_notin_H m + r d s c - /frI /(_ r d s c).
      by rewrite get_setE; case: ((m,r) = x{hr}).
    + proc; inline O2.sign. swap [2..3] -1.
      seq  2: (#pre /\ m0 = m); first by inline F.f; auto.
      auto=> /> &hr frI unI d _ c _ s _; split.
      + move=> m' @/fresh /= /negb_or [#] neq_m + r' d' s' c' - /frI fr.
        by rewrite get_set_neqE //= 1:neq_m //; exact/fr.
      move=> h' y' z' u' v' c'; rewrite get_setE.
      pose d' := (if   exists d0 s0 c0, H'.m.[m, r0] = Some (SH'Query d0 s0 c0)
                  then oapp d_get witness H'.m.[m, r0]
                  else d){hr}.
      case: (  (gg, h', y', z', u', v')
             = (gg, gg ^ d', gg ^ Game.sk, gg ^ d' ^ Game.sk,
                gg ^ (s - c * Game.sk)%PFo, gg ^ d' ^ (s - c * Game.sk)%PFo){hr})=> />.
      + by rewrite logK 1:g_n.
      by move=> _ /unI /#.
    by inline *; auto=> /> _ _ sk _; smt(emptyE).
  swap 6 - 5; inline *; auto=> /> &hr frI unI c0 _ d' _.
  case: (sig{hr})=> z r s c //=.
  rewrite !domE !get_set_sameE=> />.
  case: (H'.m.[(m, r)]{hr})=> //=.
  + case: (G'.m.[(gg, H'.gb * gg ^ d', gg ^ Game.sk, z,
                  gg ^ s * gg ^ Game.sk ^ -c, (H'.gb * gg ^ d') ^ s * z ^ -c)]{hr})=> //=.
    by move=> h' y z' u' v' c'; rewrite get_setE /#.
  move=> x _ h' y' z' u' v' c'; pose h:= h'_get H'.gb{hr} x.
  by rewrite get_setE /#.
by sim.
qed.

(** We now need to bound the following probabilities:
    [x] Pr[Game1.main() @ &m: O1.event1]
    [x] Pr[Game1.main() @ &m: O2.event2]
    [x] Pr[Game2.main() @ &m: G'.unsound]
    [x] Pr[Game2.main() @ &m: col]                     **)

(** Event 1 and Event 2 occur in the signing oracle, so we
    don't need to count the queries made during verification **)
local module CGame1 (O : GOracles) = {
  proc init() = {
                    F.init();
                    O.init();
         Game.qs <- [];
         Game.sk <$ dp;
         Game.pk <- gg ^ Game.sk;
    CountingA.cH <- 0;
    CountingA.cG <- 0;
    CountingA.cS <- 0;
  }

  (** Counting Oracles and generic game **)
  module CS = CountingA(A, O, O, O).S
  module CH = CountingA(A, O, O, O).Ho
  module CG = CountingA(A, O, O, O).Go

  proc cmain (): bool = {
    var m, sig, r, s, c, u, v, c';

                        init();
            (m, sig) <@ A(CS, CH, CG).forge(Game.pk);
    (Game.z, r, s, c) <- sig;

                    u <- gg ^ s * Game.pk ^ -c;
               Game.h <@ O.hV(m,r); (** This one doesn't count **)
                    v <- Game.h ^ s * Game.z ^ -c;
                   c' <@ O.gV(gg, Game.h, Game.pk, Game.z, u,v);

    return c = c' /\ !m \in Game.qs;
  }

  (** Bounding Oracles and generic game **)
  module BS = {
    proc sign(m) = {
      var r <- witness;

      if (CountingA.cS < qS) {
                   r <@ O.sign(m);
        CountingA.cS <- CountingA.cS + 1;
      }
      return r;
    }
  }

  module BH = {
    proc h(x) = {
      var r <- witness;

      if (CountingA.cH < qH) {
                   r <@ O.h(x);
        CountingA.cH <- CountingA.cH + 1;
      }
      return r;
    }
  }

  module BG = {
    proc g(x) = {
      var r <- witness;

      if (CountingA.cG < qG) {
                   r <@ O.g(x);
        CountingA.cG <- CountingA.cG + 1;
      }
      return r;
    }
  }

  proc bmain (): bool = {
    var m, sig, r, s, c, u, v, c';

                        init();
             (m, sig) <@ A(BS, BH, BG).forge(Game.pk);
    (Game.z, r, s, c) <- sig;

                    u <- gg ^ s * Game.pk ^ -c;
               Game.h <@ O.hV(m, r); (** This one doesn't count **)
                    v <- Game.h ^ s * Game.z ^ -c;
                   c' <@ O.gV(gg, Game.h, Game.pk, Game.z, u,v);

    return c = c' /\ !m \in Game.qs;
  }
}.

local lemma pr_Game1_counting &m E:
     Pr[Game1.main() @ &m: E O1.event1 O1.event2]
  <= Pr[CGame1(O1).bmain() @ &m: E O1.event1 O1.event2].
proof.
have ->:   Pr[Game1.main() @ &m: E O1.event1 O1.event2]
         = Pr[CGame1(O1).cmain() @ &m:    E O1.event1 O1.event2
                                       /\ CountingA.cH <= qH
                                       /\ CountingA.cG <= qG
                                       /\ CountingA.cS <= qS].
+ byequiv (: ={glob A} ==> _)=> //.
  conseq (: ={glob A} ==> ={O1.event1, O1.event2})
         _
         (: true ==> CountingA.cS <= qS /\ CountingA.cH <= qH /\ CountingA.cG <= qG)=> />.
  + proc; inline *; auto=> //=.
    by call (forge_qs (<: O1) (<: O1) (<: O1)); auto=> /#.
  proc. seq  2  2: (={O1.event1, O1.event2}); last by inline *; auto.
  call (: ={glob F, glob H', glob Gt.Lazy.LRO, Game.pk, Game.sk, O1.event1, O1.event2})=> //.
  + proc *; inline CountingA(A, O1, O1, O1).Go.g.
    by wp; sim.
  + proc *; inline CountingA(A, O1, O1, O1).Ho.h.
    by wp; sim.
  + by proc; wp; sim.
  by inline *; auto.
byequiv (: (glob A){2} = (glob A){1} /\ (glob F){2} = (glob F){1} ==> _)=> //=.
symmetry. proc.
seq  2  2: (if   (CountingA.cH <= qH /\ CountingA.cG <= qG /\ CountingA.cS <= qS){2}
            then ={glob CountingA, glob Gt.Lazy.LRO, O1.event1, O1.event2}
            else true).
+ call (: qH < CountingA.cH \/ qG < CountingA.cG \/ qS < CountingA.cS
        , ={glob CountingA, glob F, glob H', glob Gt.Lazy.LRO, O1.event1, O1.event2, Game.pk, Game.sk}).
  + exact/forge_ll.
  + proc; inline CGame1(O1).BG.g; sp; if{1}; auto.
    + by call (: ={glob Gt.Lazy.LRO}); first by sim.
    by inline *; auto; rewrite dp_ll=> /#.
  + by move=> &2 bad; islossless; exact/dp_ll.
  + by move=> _; proc; wp; call (: true); auto=> [|/#]; by rewrite dp_ll.
  + proc; inline CGame1(O1).BH.h; sp; if{1}; auto.
    + by call (: ={glob H'}); first by sim.
    by inline *; auto; rewrite dp_ll=> /#.
  + by move=> &2 bad; islossless; exact/dp_ll.
  + by move=> _; proc; wp; call (: true); auto=> [|/#]; by rewrite dp_ll.
  + proc; inline CGame1(O1).BS.sign. case: (CountingA.cS{1} < qS).
    + rcondt{1} 2; first by auto.
      wp.
      call (: ={glob F, glob H', glob Gt.Lazy.LRO, Game.pk, Game.sk, O1.event1, O1.event2})=> //.
      + by sim.
      by auto.
    wp; conseq (: true)=> [/#|].
    by inline *; sp; if{1}; auto; rewrite drand_ll dp_ll.
  + move=> &2 bad.
    proc; sp; if=> //=; wp; call (: true)=> //.
    auto; call (: true).
    + by auto; rewrite dp_ll.
    auto; call (: true).
    + by auto; rewrite drand_ll.
    by auto; rewrite dp_ll.
  + move=> _; proc; wp; call (: true); inline *; auto=> [|/#].
    by rewrite dp_ll drand_ll.
  by inline *; auto=> /#.
by conseq (: true)=> [/#|]; inline *; auto.
qed.

local lemma pr_Game1_event1 &m:
     Pr[Game1.main() @ &m: O1.event1]
  <= qS%r * qH%r * drand_max.
proof.
move: (pr_Game1_counting &m (fun x _=> x))=> /= /ler_trans; apply.
fel 1 CountingA.cS
      (fun i=> qH%r * drand_max)
      qS
      O1.event1
      [CGame1(O1).BH.h   : false;
       CGame1(O1).BS.sign: (CountingA.cS < qS)]
      (   CountingA.cH <= qH
       /\ CountingA.cS <= qS
       /\ card (fdom (filter (fun _ x=> exists d, x = AH'Query d) H'.m)) <= CountingA.cH
       /\ (forall m r, Rt.Lazy.LRO.m.[m] = Some r => exists d s c, H'.m.[m, r] = Some (SH'Query d s c)))=> //=.
+ rewrite -mulr_suml StdBigop.Bigreal.sumr_const.
  by rewrite count_predT size_range /=; have ->: (max 0 qS)%r = qS%r by smt(ge0_qS).
+ inline *; auto=> /> _ _ _ _; rewrite ge0_qH ge0_qS /=.
  have ->: filter (fun (_ : msg * rand) y=> exists d, y = AH'Query d) empty = empty.
  + by apply: fmap_eqP=> x; rewrite filterE emptyE.
  by rewrite fdom0 fcards0=> /= m r; rewrite emptyE.
+ move=> b c; proc; sp; if=> //=.
  inline *; auto=> /> &hr _ cS_qS size_domH2 inv cH_qH d _; split=> [|/#].
  rewrite filter_set /=.
  have -> /=: exists d0, d = d0; first by exists d.
  rewrite fdom_set fcardU.
  move=> x_notin_H'; do !split=> [/#||].
  + smt(fcard1 fcard_ge0).
  move=> m r /inv [] d0 s0 c0 H'_mr.
  rewrite get_set_neqE.
  + by rewrite -negP=> <<-; move: x_notin_H'; rewrite domE H'_mr.
  by exists d0 s0 c0.
+ proc; rcondt 2; first by auto.
  inline O1.sign. swap 14 -10.
  seq  4: O1.event1 (qH%r * drand_max) 1%r _ 0%r=> //=.
  + inline F.f.
    case: (m \in Rt.Lazy.LRO.m).
    + hoare; first smt(ge0_qH gt0_drand_max).
      rcondf 5; first by auto=> /#.
      auto=> /> &hr _ _ _ _ _ _ /(_ m{hr}); rewrite domE.
      by case: (Lazy.LRO.m.[m]{hr})=> /> r /(_ r) /> d0 s0 c0 ->.
    rcondt 5; first by auto=> /#.
    wp; rnd (mem (map snd (elems (fdom (filter (fun (_ : msg * rand) y=> exists d, y = AH'Query d) H'.m))))).
    auto=> /> &m' _ _ _ cH_qH cS_qS size_H2 _ _; split=> [|_].
    + apply/(ler_trans ((card (fdom (filter (fun (_ : msg * rand) y=> exists d, y = AH'Query d) H'.m{m'})))%r * drand_max)).
      + rewrite cardE -(@size_map snd); apply/mu_mem_le_size=> x _.
        by rewrite drand_max_ub.
      apply/ler_pmul2r; first smt(gt0_drand_max).
      exact/le_fromint/(@lez_trans _ _ _ size_H2).
    move=> r _ d; rewrite get_set_sameE=> /= H'r.
    rewrite mapP. exists (m{m'},r)=> /=.
    rewrite -memE mem_fdom domE filterE H'r /=.
    by have ->: exists d0, d = d0; first by exists d.
  by hoare; inline *; auto.
+ move=> c; proc; sp; if=> //=; inline O1.sign G.g; auto=> /=.
  conseq (:    Lazy.LRO.m.[m0] = Some r0
            /\ (forall m r, Lazy.LRO.m.[m] = Some r => ((m = m0 /\ r = r0) \/ exists d s c, H'.m.[m, r] = Some (SH'Query d s c)))).
  + auto=> /> &hr cS_qS cH_qH _ size_H2 inv0 F m r Fm inv k _ d _ c' _.
    pose d':= if   exists d s c, H'.m.[m, r]{hr} = Some (SH'Query d s c)
              then oapp d_get witness H'.m{hr}.[m, r]
              else d.
    rewrite !get_set_sameE /=.
    have -> /= : c < c + 1; first smt().
    have -> /= : c + 1 <= qS; first smt().
    rewrite domE.
    case: (Gt.Lazy.LRO.m.[gg, gg ^ d', Game.pk, gg ^ d' ^ Game.sk, gg ^ k, gg ^ d' ^ k]{hr})=> />.
    + split.
      + by rewrite filter_set /= fdom_rem fcardD; smt(fcard_ge0).
      move=> m' r' /inv; rewrite get_setE /=; case: (m' = m /\ r' = r)=> [/>|//].
      by exists d' (k + c' * Game.sk{hr}) c'.
    move=> c0; split.
    + by rewrite filter_set /= fdom_rem fcardD; smt(fcard_ge0).
    move=> m' r' /inv; rewrite get_setE /=; case: (m' = m /\ r' = r)=> [/>|//].
    by exists d' (k + c0 * Game.sk{hr}) c0.
  inline *; auto=> /> &hr _ _ _ _ inv r _; rewrite get_set_sameE /= domE.
  case: {-1}(Lazy.LRO.m.[m]{hr}) (eq_refl Lazy.LRO.m.[m]{hr}) (inv m{hr})=> //= [Fm m' r'|r' Fm /(_ r') /> d' s' c' H'm m0 r0].
  + by rewrite get_setE; case: (m' = m{hr})=> /> _ /inv.
  case: (m0 = m{hr})=> /> => [|_ /inv //].
  by rewrite Fm.
by move=> b c; proc; sp; rcondf 1; auto.
qed.

local lemma pr_Game1_event2 &m:
     Pr[Game1.main() @ &m: O1.event2]
  <= qS%r * (qG%r + (qS - 1)%r / 2%r) / q%r.
proof.
move: (pr_Game1_counting &m (fun _ y=> y))=> /= /ler_trans; apply.
fel 1 CountingA.cS
      (fun i=> (qG%r + i%r) / q%r)
      qS
      O1.event2
      [CGame1(O1).BG.g   : false;
       CGame1(O1).BS.sign: (CountingA.cS < qS)]
      (   CountingA.cG <= qG
       /\ CountingA.cS <= qS
       /\ card (fdom Gt.Lazy.LRO.m) <= CountingA.cG + CountingA.cS)=> //=.
+ rewrite -mulr_suml -sumrD StdBigop.Bigreal.sumr_const.
  rewrite count_predT size_range /=; have ->: (max 0 qS)%r = qS%r by smt(ge0_qS).
  rewrite StdBigop.Bigreal.sumidE 1:ge0_qS.
  by rewrite fromintM -Real.RField.mulrA -Real.RField.mulrDr.
+ by inline *; auto=> /> _ _ _ _; rewrite ge0_qG ge0_qS /= fdom0 fcards0.
+ move=> b c; proc; inline *; sp; if; auto.
  move=> /> &hr _ cS_qS size_H3 cH'_qH' d _; split=> [|/#].
  by rewrite fdom_set fcardU fcard1 #smt:(fcard_ge0).
+ proc; rcondt 2; first by auto.
  conseq (: _ ==> _: <= ((qG%r + CountingA.cS%r) / q%r))=> //=.
  seq  1: (!O1.event2) 1%r ((qG%r + CountingA.cS%r) / q%r) 0%r _
          (   card (fdom Gt.Lazy.LRO.m) <= CountingA.cG + CountingA.cS
           /\ CountingA.cG <= qG)=> //=; last first.
  + by hoare; auto.
  + by auto.  
  wp; call (:    !O1.event2
              /\ card (fdom Gt.Lazy.LRO.m) <= qG + CountingA.cS
              ==> O1.event2)=> //=.
  + proc.
    seq  9: O1.event2 ((qG%r + CountingA.cS%r) / q%r) 1%r _ 0%r
            (card (fdom Gt.Lazy.LRO.m) <= qG + CountingA.cS)=> //=.
    + by inline *; auto.
    + swap 4 -2.
      wp; conseq (: _ ==> k \in map (fun (x : _ * _ * _ * _ * _ * _)=> log gg x.`5)
                                    (elems (fdom Gt.Lazy.LRO.m))).
      + move=> /> &2 size_G _ d k r; rewrite -mem_fdom memE mapP.
        pose d' := (if   exists d s c, H'.m.[m, r] = Some (SH'Query d s c)
                    then oapp d_get witness H'.m.[m, r]
                    else d){2}.
        move=> sig_in.
        exists (gg, gg ^ d', gg ^ Game.sk, gg ^ d' ^ Game.sk, gg ^ k, gg ^ d' ^ k){2}=> //=.
        by rewrite !logK 1:g_n.
      rnd (mem (map (fun (x : _GP * _GP * _GP * _GP * _GP * _GP)=> log gg x.`5)
                    (elems (fdom Gt.Lazy.LRO.m))))=> //=.
      conseq (: true).
      + auto=> /> &2 _ size_G.
        pose proj:= fun (x : _GP * _GP * _GP * _GP * _GP * _GP)=> log gg x.`5.
        move: (mu_mem_le_size (map proj (elems (fdom Gt.Lazy.LRO.m{2}))) dp (inv q%r) _).
        + by move=> k _; rewrite dp1E.
        rewrite size_map -cardE=> /ler_trans h; apply/h.
        apply/ler_pmul2r.
        + smt(ge2_q).
        rewrite -fromintD; exact/le_fromint.
      by inline *; auto.
    by hoare; inline *; auto.
  by auto=> /#.
+ move=> c; proc; rcondt 2; first by auto.
  inline O1.sign; seq  3: #pre; first by inline *; auto.
  inline *; auto=> /> &hr cS_qS cG_qG _ size_G k _ d _ c' _ /=.
  by rewrite fdom_set fcardU fcard1 #smt:(fcard_ge0).
by move=> b c; proc; rcondf 2; auto.
qed.

local phoare ph_G'_unsound: [G'.g: !G'.unsound ==> G'.unsound] <= (inv q%r).
proof.
proc=> />.
case: ((x \in G'.m){hr}).
+ rcondf 2=> />; first by auto.
  by hoare=> //= _ _; rewrite invr_ge0 le_fromint; smt(ge2_q).
rcondt 2=> />; first by auto.
wp; rnd (fun c=>   x.`4 <> x.`2 ^ log gg x.`3
                /\ (x.`5 * x.`3 ^ c) ^ log gg x.`2 = x.`6 * x.`4 ^ c ).
skip=> /> &hr; case: (x{hr})=> g h y z u v /= notH3 _.
apply/(ler_trans (mu1 dp (((log gg v) + (-(log gg h) * (log gg u))) * inv ((log gg h) * (log gg y) + (- (log gg z)))))).
+ rewrite mu_sub=> @/pred1 c [#] z_neq_hx.
  pose lh := log gg h. pose ly := log gg y. pose lz := log gg z.
  pose lu := log gg u. pose lv := log gg v.
  move=> /(congr1 (log gg)); rewrite !(logM, logD) ?g_n // -/ly -/lz -/lu -/lv.
  rewrite ZModpRing.mulrDr ZModpRing.addrC -ZModpRing.eqr_sub.
  rewrite -ZModpRing.mulrN ZModpRing.mulrCA -ZModpRing.mulrDr.
  move=> <-; rewrite -ZModpRing.mulrA ZModpField.mulrV.
  + move: z_neq_hx; apply/contra.
    rewrite ZModpRing.addr_eq0 ZModpRing.opprK=> /(congr1 (fun x=> gg ^ x)) /=.
    move: powK=> @/(^) powK.
    by rewrite /(^) mulE -intpow_mod -expMz /lh /ly /lz /lu /lv powK 1:g_n=> // ->; rewrite powK 1:g_n.
  by rewrite ZModpRing.mulr1.
by rewrite dp1E.
qed.

(** On the other hand, the simulator can exploit unsoundness stemming
    from the forgery itself, so we do need to count the G' query made
    during verification.                                              **)
local module CGame2 (O : GOracles) = {
  proc init() = {
                    F.init();
                    O.init();
         Game.qs <- [];
         Game.sk <$ dp;
         Game.pk <- gg ^ Game.sk;
    CountingA.cH <- 0;
    CountingA.cG <- 0;
    CountingA.cS <- 0;
  }

  (** Counting Oracles and generic game **)
  module CS = CountingA(A, O, O, O).S
  module CH = CountingA(A, O, O, O).Ho
  module CG = CountingA(A, O, O, O).Go

  proc gV   = CG.g

  proc cmain (): bool = {
    var m, sig, r, s, c, u, v, c';

                         init();
             (m, sig) <@ A(CS, CH, CG).forge(Game.pk);
    (Game.z, r, s, c) <- sig;

                    u <- gg ^ s * Game.pk ^ -c;
               Game.h <@ O.hV(m, r); (** This one doesn't count **)
                    v <- Game.h ^ s * Game.z ^ -c;
                   c' <@ gV(gg, Game.h, Game.pk, Game.z, u,v);

    return c = c' /\ !m \in Game.qs;
  }

  (** Bounding Oracles and generic game **)
  module BS = {
    proc sign(m) = {
      var r <- witness;

      if (CountingA.cS < qS) {
                   r <@ O.sign(m);
        CountingA.cS <- CountingA.cS + 1;
      }
      return r;
    }
  }

  module BH = {
    proc h(x) = {
      var r <- witness;

      if (CountingA.cH < qH) {
                   r <@ O.h(x);
        CountingA.cH <- CountingA.cH + 1;
      }
      return r;
    }
  }

  module BG = {
    proc g(x) = {
      var r <- witness;

      if (CountingA.cG < qG) {
                   r <@ O.g(x);
        CountingA.cG <- CountingA.cG + 1;
      }
      return r;
    }

    proc gV(x) = {
      var r <- witness;

      if (CountingA.cG <= qG) {
                   r <@ O.g(x);
        CountingA.cG <- CountingA.cG + 1;
      }
      return r;
    }

  }

  proc bmain (): bool = {
    var m, sig, r, s, c, u, v, c';

                         init();
             (m, sig) <@ A(BS, BH, BG).forge(Game.pk);
    (Game.z, r, s, c) <- sig;

                    u <- gg ^ s * Game.pk ^ -c;
               Game.h <@ O.hV(m, r); (** This one doesn't count **)
                    v <- Game.h ^ s * Game.z ^ -c;
                   c' <@ BG.gV(gg, Game.h, Game.pk, Game.z, u,v);

    return c = c' /\ !m \in Game.qs;
  }
}.

local lemma pr_Game2_counting P &m:
     Pr[Game2.main() @ &m: P G'.unsound G'.scol G'.gcol]
  <= Pr[CGame2(O2).bmain() @ &m: P G'.unsound G'.scol G'.gcol].
proof.
have ->:   Pr[Game2.main() @ &m: P G'.unsound G'.scol G'.gcol]
         = Pr[CGame2(O2).cmain() @ &m:    P G'.unsound G'.scol G'.gcol
                                       /\ CountingA.cH <= qH
                                       /\ CountingA.cG <= qG + 1
                                       /\ CountingA.cS <= qS].
+ byequiv (: ={glob A} ==> _)=> //.
  conseq (: ={glob A} ==> ={G'.unsound, G'.scol, G'.gcol})
         _
         (: true ==> CountingA.cS <= qS /\ CountingA.cH <= qH /\ CountingA.cG <= qG + 1)=> />.
  + proc; inline *; auto=> //=.
    by call (forge_qs (<: O2) (<: O2) (<: O2)); auto=> /#.
  proc. seq  2  2: (={glob F, glob H', glob G', Game.pk, Game.sk, m, sig}); last first.
  + by inline *; auto.
  call (: ={glob F, glob H', glob G', Game.pk, Game.sk})=> //.
  + proc *; inline CountingA(A, O2, O2, O2).Go.g.
    by wp; sim.
  + proc *; inline CountingA(A, O2, O2, O2).Ho.h.
    by wp; sim.
  + by proc; wp; sim.
  by inline *; auto.
byequiv (: (glob A){2} = (glob A){1} /\ (glob F){2} = (glob F){1} ==> _)=> //=.
symmetry. proc.
seq  2  2: (if   (CountingA.cH <= qH /\ CountingA.cG <= qG /\ CountingA.cS <= qS){2}
            then ={glob CountingA, glob H', glob G', Game.pk, Game.sk, m, sig}
            else true).
+ call (: qH < CountingA.cH \/ qG < CountingA.cG \/ qS < CountingA.cS
        , ={glob CountingA, glob F, glob H', glob G', Game.pk, Game.sk}).
  + exact/forge_ll.
  + proc; inline CGame2(O2).BG.g; sp; if{1}; auto.
    + by call (: ={glob G'}); first by sim.
    by inline *; auto; rewrite dp_ll=> /#.
  + by move=> &2 bad; islossless; exact/dp_ll.
  + by move=> _; proc; wp; call (: true); auto=> [|/#]; by rewrite dp_ll.
  + proc; inline CGame2(O2).BH.h; sp; if{1}; auto.
    + by call (: ={glob H'}); first by sim.
    by inline *; auto; rewrite dp_ll=> /#.
  + by move=> &2 bad; islossless; exact/dp_ll.
  + by move=> _; proc; wp; call (: true); auto=> [|/#]; by rewrite dp_ll.
  + proc; inline CGame2(O2).BS.sign; sp; if{1}; auto.
    + call (: ={glob F, glob H', glob G', Game.pk, Game.sk})=> //.
      by sim.
    by inline *; auto; rewrite dp_ll drand_ll /#.
  + by move=> &2 bad; islossless; rewrite ?dp_ll ?drand_ll.
  + move=> _; proc; wp; call (: true); inline *; auto=> [|/#].
    by rewrite dp_ll drand_ll.
  by inline *; auto=> /#.
case: (CountingA.cG{1} <= qG).
+ by inline *; rcondt{1} 10; auto=> /#.
inline *; rcondf{1} 10; first by auto=> /#.
by auto; rewrite dp_ll=> /#.
qed.

local lemma pr_Game2_unsound &m:
  Pr[Game2.main() @ &m: G'.unsound] <= (qG + 1)%r / q%r.
proof.
move: (pr_Game2_counting (fun x _ _=> x) &m)=> /= /ler_trans; apply.
fel 1 CountingA.cG
      (fun _=> inv q%r)
      (qG + 1)
      G'.unsound
      [CGame2(O2).BS.sign: false;
       CGame2(O2).BG.g   : (CountingA.cG < qG);
       CGame2(O2).BG.gV  : (CountingA.cG <= qG)]
      (CountingA.cG <= qG + 1) => //=.
+ by rewrite StdBigop.Bigreal.sumr_const count_predT size_range /=; smt(ge0_qG ge2_q). 
+ by inline*; auto=> />; smt(ge0_qG).
+ proc; sp; if; auto.
  by call ph_G'_unsound.
+ by move=> c; proc; inline *; sp; if=> //=; auto=> /#.
+ by move=> b c; proc; inline *; sp; if=> //=; auto.
+ proc; sp; if; sp; auto.
  by call ph_G'_unsound.
+ by move=> c; proc; inline *; sp; if=> //=; auto=> /#.
by move=> b c; proc; inline*; sp; if; auto=> /#.
qed.

local phoare pr_G'_gcol:
  [G'.g:    !G'.gcol
         /\ card (fdom (filter (fun _ gr=> exists c, gr = SG'Query c) G'.m)) <= qS
         ==> G'.gcol] <= (qS%r / q%r).
proof.
proc. case: (x \in G'.m).
+ hoare; first smt(ge0_qS ge2_q).
  by auto.
rcondt 2; first by auto.
wp; rnd (fun x=> x \in frng (map (fun _=> c_get) (filter (fun _ gr=> exists c, gr = SG'Query c) G'.m))).
auto=> /> &hr _.
pose sG := filter _ G'.m{hr}.
move=> size_sG [#] x_notin_G; split=> [|_].
+ have:= mu_mem_le (frng (map (fun _=> c_get) sG)) dp (inv q%r) _.
  + by move=> x _; rewrite dp1E.
  have /le_fromint := le_card_frng_fdom sG.
  have /le_fromint := fcard_image_leq c_get (frng sG).
  have <-: frng (map (fun _=> c_get) sG) = image c_get (frng sG).
  + apply/fsetP=> c; rewrite mem_frng rngE imageP /=; split.
    + move=> [x]; rewrite mapE; case: {-1}(sG.[x]) (eq_refl (sG.[x]))=> //=.
      move=> gr /> sG_x; exists gr=> //=; rewrite mem_frng rngE /=.
      by exists x.
    move=> [gr] />; rewrite mem_frng rngE=> /= - [x] sG_x.
    by exists x; rewrite mapE sG_x.
  move=> size_rngs size_rng_dom tight.
  apply: (@ler_trans _ _ _ tight).
  apply: ler_pmul2r.
  + by apply/invr_gt0/lt_fromint; smt(ge2_q).
  exact/(@ler_trans _ _ _ size_rngs)/(@ler_trans _ _ _ size_rng_dom)/le_fromint.
move=> c _ x G_x. rewrite mem_frng rngE /=.
exists x; rewrite mapE /sG /= filterE /= G_x /=.
by have ->: exists c0, c = c0; first by exists c.
qed.

lemma Security &m:
  Pr[EUF_CMA(EDL_Sig, H, G, A).main() @ &m: res]
  <=   Pr[CDH(Simulator(A)).main() @ &m: res]
     + qS%r * qH%r * drand_max
     + qS%r * (qG%r + (qS - 1)%r / 2%r) / q%r
     + (qG + 1)%r / q%r.
proof.
have:= Reduction &m.
have:= pr_Game2_unsound &m.
have:= pr_Game1_event2 &m.
have:= pr_Game1_event1 &m.
have:= pr_Game1_Game2 &m.
have:= pr_CMA_Game1 &m.
smt().
qed.

end section Proofs.
