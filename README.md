Please follow EasyCrypt's [installation
instructions](https://github.com/EasyCrypt/easycrypt/blob/1.0/README.md) to
install EasyCrypt release `r2022.04`. Also install some SMT solvers. (We
recommend Z3, Alt-Ergo and CVC4.)

You can then run EasyCrypt on the proof scripts by running, at the root of this
repository:

    make check

This will check all proofs, including those for EDL, CM and GJKW, and a bonus
proof for a PRF-based deterministic version of EDL.

Troubleshooting
--------------------------------------------------------------------

Due to differences in performance, it might be necessary to change the timeout
for SMT solvers (which is set to 3s by default). To do so, set the ECARGS
environment variable to `-timeout X`, where X is your chosen timeout. We have
found that a timeout of 30 suffices in most reasonable cases although it does
affect verification time significantly.

